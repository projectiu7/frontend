(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['playlistTabs.hbs'] = template({"1":function(container,depth0,helpers,partials,data,blockParams,depths) {
    var stack1, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return ((stack1 = container.invokePartial(lookupProperty(partials,"playlistTab"),depth0,{"name":"playlistTab","hash":{"username":(depths[1] != null ? lookupProperty(depths[1],"username") : depths[1])},"data":data,"indent":"            ","helpers":helpers,"partials":partials,"decorators":container.decorators})) != null ? stack1 : "");
},"3":function(container,depth0,helpers,partials,data) {
    return "        <button id=\"create-playlist-button\" class=\"fa-button create-playlist-button create-playlist-button_size_l\">\n            <i class=\"fa fa-plus-circle\" aria-hidden=\"true\"></i>\n            Создать плейлист\n        </button>\n        <div id=\"create-playlist-container\" class=\"create-playlist-container create-playlist-container_hidden\">\n            <input id=\"input-create-playlist\" name=\"playlistName\" type=\"text\"\n                   class=\"create-playlist-container__input create-playlist-container__input_size_l\"\n                   placeholder=\"Название плейлиста\">\n            <button id=\"submit-create-playlist\"\n                    class=\"fa-button create-playlist-container__button create-playlist-container__button_size_l\">\n                <i class=\"fa fa-arrow-circle-o-right\" aria-hidden=\"true\"></i>\n            </button>\n            <button id=\"cancel-create-playlist\"\n                    class=\"fa-button create-playlist-container__button create-playlist-container__button_size_l\">\n                <i class=\"fa fa-times-circle-o\" aria-hidden=\"true\"></i>\n            </button>\n        </div>\n";
},"compiler":[8,">= 4.3.0"],"main":function(container,depth0,helpers,partials,data,blockParams,depths) {
    var stack1, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=container.hooks.helperMissing, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "<div class=\"main__accordion\">\n    <div id=\"tabs\">\n"
    + ((stack1 = lookupProperty(helpers,"each").call(alias1,(depth0 != null ? lookupProperty(depth0,"playlists") : depth0),{"name":"each","hash":{},"fn":container.program(1, data, 0, blockParams, depths),"inverse":container.noop,"data":data,"loc":{"start":{"line":3,"column":8},"end":{"line":5,"column":17}}})) != null ? stack1 : "")
    + "    </div>\n"
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(lookupProperty(helpers,"eq")||(depth0 && lookupProperty(depth0,"eq"))||alias2).call(alias1,(depth0 != null ? lookupProperty(depth0,"username") : depth0),(lookupProperty(helpers,"getUsername")||(depth0 && lookupProperty(depth0,"getUsername"))||alias2).call(alias1,{"name":"getUsername","hash":{},"data":data,"loc":{"start":{"line":7,"column":23},"end":{"line":7,"column":36}}}),{"name":"eq","hash":{},"data":data,"loc":{"start":{"line":7,"column":10},"end":{"line":7,"column":37}}}),{"name":"if","hash":{},"fn":container.program(3, data, 0, blockParams, depths),"inverse":container.noop,"data":data,"loc":{"start":{"line":7,"column":4},"end":{"line":25,"column":11}}})) != null ? stack1 : "")
    + "</div>\n";
},"usePartial":true,"useData":true,"useDepths":true});
})();