(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['addToPlaylistWidget.hbs'] = template({"1":function(container,depth0,helpers,partials,data,blockParams,depths) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=container.hooks.helperMissing, alias3="function", alias4=container.escapeExpression, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "            <div class=\"add-to-playlist-widget__playlist-row\">\n                <label class=\"add-to-playlist-widget__label\" data-playlist-id=\""
    + alias4(((helper = (helper = lookupProperty(helpers,"id") || (depth0 != null ? lookupProperty(depth0,"id") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"id","hash":{},"data":data,"loc":{"start":{"line":6,"column":79},"end":{"line":6,"column":85}}}) : helper)))
    + "\" for=\"checkbox-"
    + alias4(((helper = (helper = lookupProperty(helpers,"id") || (depth0 != null ? lookupProperty(depth0,"id") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"id","hash":{},"data":data,"loc":{"start":{"line":6,"column":101},"end":{"line":6,"column":107}}}) : helper)))
    + "\">\n                    "
    + alias4(((helper = (helper = lookupProperty(helpers,"playlist_name") || (depth0 != null ? lookupProperty(depth0,"playlist_name") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"playlist_name","hash":{},"data":data,"loc":{"start":{"line":7,"column":20},"end":{"line":7,"column":37}}}) : helper)))
    + "\n                </label>\n"
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"is_added") : depth0),{"name":"if","hash":{},"fn":container.program(2, data, 0, blockParams, depths),"inverse":container.program(4, data, 0, blockParams, depths),"data":data,"loc":{"start":{"line":9,"column":16},"end":{"line":15,"column":23}}})) != null ? stack1 : "")
    + "            </div>\n";
},"2":function(container,depth0,helpers,partials,data,blockParams,depths) {
    var helper, alias1=container.escapeExpression, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "                    <input id=\"checkbox-"
    + alias1(((helper = (helper = lookupProperty(helpers,"id") || (depth0 != null ? lookupProperty(depth0,"id") : depth0)) != null ? helper : container.hooks.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"id","hash":{},"data":data,"loc":{"start":{"line":10,"column":40},"end":{"line":10,"column":46}}}) : helper)))
    + "\" class=\"add-to-playlist-widget__checkbox\" type=\"checkbox\"\n                           data-movie-id=\""
    + alias1(container.lambda((depths[1] != null ? lookupProperty(depths[1],"movieId") : depths[1]), depth0))
    + "\" checked/>\n";
},"4":function(container,depth0,helpers,partials,data,blockParams,depths) {
    var helper, alias1=container.escapeExpression, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "                    <input id=\"checkbox-"
    + alias1(((helper = (helper = lookupProperty(helpers,"id") || (depth0 != null ? lookupProperty(depth0,"id") : depth0)) != null ? helper : container.hooks.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"id","hash":{},"data":data,"loc":{"start":{"line":13,"column":40},"end":{"line":13,"column":46}}}) : helper)))
    + "\" class=\"add-to-playlist-widget__checkbox\" type=\"checkbox\"\n                           data-movie-id=\""
    + alias1(container.lambda((depths[1] != null ? lookupProperty(depths[1],"movieId") : depths[1]), depth0))
    + "\"/>\n";
},"6":function(container,depth0,helpers,partials,data) {
    return "        <div class=\"main__message-empty\">\n            <p id=\"no-playlists-message\" class=\"message-empty__text\">\n                Создайте свой первый плейлист!\n            </p>\n        </div>\n";
},"compiler":[8,">= 4.3.0"],"main":function(container,depth0,helpers,partials,data,blockParams,depths) {
    var stack1, alias1=depth0 != null ? depth0 : (container.nullContext || {}), lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "<div class=\"add-to-playlist-widget\">\n    <h3 class=\"add-to-playlist-widget__title\">Добавить в плейлист</h3>\n    <div id=\"playlists-list\">\n"
    + ((stack1 = lookupProperty(helpers,"each").call(alias1,(depth0 != null ? lookupProperty(depth0,"playlists") : depth0),{"name":"each","hash":{},"fn":container.program(1, data, 0, blockParams, depths),"inverse":container.noop,"data":data,"loc":{"start":{"line":4,"column":8},"end":{"line":17,"column":17}}})) != null ? stack1 : "")
    + "    </div>\n"
    + ((stack1 = lookupProperty(helpers,"unless").call(alias1,(depth0 != null ? lookupProperty(depth0,"playlists") : depth0),{"name":"unless","hash":{},"fn":container.program(6, data, 0, blockParams, depths),"inverse":container.noop,"data":data,"loc":{"start":{"line":19,"column":4},"end":{"line":25,"column":15}}})) != null ? stack1 : "")
    + "\n    <button id=\"create-playlist-button\" class=\"fa-button create-playlist-button create-playlist-button_size_s\">\n        <i class=\"fa fa-plus-circle\" aria-hidden=\"true\"></i>\n        Создать плейлист\n    </button>\n    <div id=\"create-playlist-container\" class=\"create-playlist-container create-playlist-container_hidden\">\n        <input id=\"input-create-playlist\" name=\"playlistName\" type=\"text\"\n               class=\"create-playlist-container__input create-playlist-container__input_size_s\"\n               placeholder=\"Название плейлиста\">\n        <button id=\"submit-create-playlist\"\n                class=\"fa-button create-playlist-container__button create-playlist-container__button_size_s\">\n            <i class=\"fa fa-arrow-circle-o-right\" aria-hidden=\"true\"></i>\n        </button>\n        <button id=\"cancel-create-playlist\"\n                class=\"fa-button create-playlist-container__button create-playlist-container__button_size_s\">\n            <i class=\"fa fa-times-circle-o\" aria-hidden=\"true\"></i>\n        </button>\n    </div>\n</div>\n";
},"useData":true,"useDepths":true});
})();