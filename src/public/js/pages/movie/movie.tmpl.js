(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['movie.hbs'] = template({"1":function(container,depth0,helpers,partials,data) {
    var stack1, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "                            <a href=\"javascript:void(0);\" class=\"info-row__info-link\">"
    + container.escapeExpression(container.lambda(depth0, depth0))
    + "</a>"
    + ((stack1 = lookupProperty(helpers,"unless").call(depth0 != null ? depth0 : (container.nullContext || {}),(data && lookupProperty(data,"last")),{"name":"unless","hash":{},"fn":container.program(2, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":42,"column":98},"end":{"line":42,"column":128}}})) != null ? stack1 : "")
    + "\n";
},"2":function(container,depth0,helpers,partials,data) {
    return ", ";
},"4":function(container,depth0,helpers,partials,data) {
    var stack1, alias1=container.lambda, alias2=container.escapeExpression, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "                            <a href=\"/movies/genre/1/?filter="
    + alias2(alias1(depth0, depth0))
    + "\" class=\"info-row__info-link\">"
    + alias2(alias1(depth0, depth0))
    + "</a>"
    + ((stack1 = lookupProperty(helpers,"unless").call(depth0 != null ? depth0 : (container.nullContext || {}),(data && lookupProperty(data,"last")),{"name":"unless","hash":{},"fn":container.program(5, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":48,"column":111},"end":{"line":48,"column":154}}})) != null ? stack1 : "")
    + "\n";
},"5":function(container,depth0,helpers,partials,data) {
    return "<span>, </span>";
},"7":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=container.hooks.helperMissing, alias3="function", alias4=container.escapeExpression, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "                    <div class=\"movie-flex-right__buttons\">\n"
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"is_watched") : depth0),{"name":"if","hash":{},"fn":container.program(8, data, 0),"inverse":container.program(10, data, 0),"data":data,"loc":{"start":{"line":96,"column":24},"end":{"line":102,"column":31}}})) != null ? stack1 : "")
    + "                        <label class=\"buttons__label-watched\" for=\"watched-button\"></label>\n                        <input class=\"buttons__input-playlist buttons__movie-page-input-playlist\"\n                               id=\"playlist-button\" type=\"checkbox\" name=\"playlist\" data-id=\""
    + alias4(((helper = (helper = lookupProperty(helpers,"id") || (depth0 != null ? lookupProperty(depth0,"id") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"id","hash":{},"data":data,"loc":{"start":{"line":105,"column":93},"end":{"line":105,"column":99}}}) : helper)))
    + "\">\n                        <label class=\"buttons__label-playlist\" for=\"playlist-button\"></label>\n                    </div>\n                    <div class=\"buttons__add-to-playlist-container\" id=\"add-to-playlist-"
    + alias4(((helper = (helper = lookupProperty(helpers,"id") || (depth0 != null ? lookupProperty(depth0,"id") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"id","hash":{},"data":data,"loc":{"start":{"line":108,"column":88},"end":{"line":108,"column":94}}}) : helper)))
    + "\"></div>\n";
},"8":function(container,depth0,helpers,partials,data) {
    var helper, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "                            <input class=\"buttons__input-watched buttons__movie-card-input-watched\" checked\n                                   id=\"watched-button\" type=\"checkbox\" name=\"watched\" data-id=\""
    + container.escapeExpression(((helper = (helper = lookupProperty(helpers,"id") || (depth0 != null ? lookupProperty(depth0,"id") : depth0)) != null ? helper : container.hooks.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"id","hash":{},"data":data,"loc":{"start":{"line":98,"column":95},"end":{"line":98,"column":101}}}) : helper)))
    + "\">\n";
},"10":function(container,depth0,helpers,partials,data) {
    var helper, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "                            <input class=\"buttons__input-watched buttons__movie-card-input-watched\"\n                                   id=\"watched-button\" type=\"checkbox\" name=\"watched\" data-id=\""
    + container.escapeExpression(((helper = (helper = lookupProperty(helpers,"id") || (depth0 != null ? lookupProperty(depth0,"id") : depth0)) != null ? helper : container.hooks.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"id","hash":{},"data":data,"loc":{"start":{"line":101,"column":95},"end":{"line":101,"column":101}}}) : helper)))
    + "\">\n";
},"12":function(container,depth0,helpers,partials,data) {
    var helper, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "<h2 class=\"right__movie-rating positive-rating\">"
    + container.escapeExpression(((helper = (helper = lookupProperty(helpers,"rating") || (depth0 != null ? lookupProperty(depth0,"rating") : depth0)) != null ? helper : container.hooks.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"rating","hash":{},"data":data,"loc":{"start":{"line":111,"column":86},"end":{"line":111,"column":96}}}) : helper)))
    + "</h2>\n";
},"14":function(container,depth0,helpers,partials,data) {
    var stack1, alias1=depth0 != null ? depth0 : (container.nullContext || {}), lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "                    "
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(lookupProperty(helpers,"gte")||(depth0 && lookupProperty(depth0,"gte"))||container.hooks.helperMissing).call(alias1,(depth0 != null ? lookupProperty(depth0,"rating") : depth0),4,{"name":"gte","hash":{},"data":data,"loc":{"start":{"line":113,"column":26},"end":{"line":113,"column":40}}}),{"name":"if","hash":{},"fn":container.program(15, data, 0),"inverse":container.program(17, data, 0),"data":data,"loc":{"start":{"line":113,"column":20},"end":{"line":114,"column":98}}})) != null ? stack1 : "")
    + "\n";
},"15":function(container,depth0,helpers,partials,data) {
    var helper, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "<h2 class=\"right__movie-rating neutral-rating\">"
    + container.escapeExpression(((helper = (helper = lookupProperty(helpers,"rating") || (depth0 != null ? lookupProperty(depth0,"rating") : depth0)) != null ? helper : container.hooks.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"rating","hash":{},"data":data,"loc":{"start":{"line":113,"column":89},"end":{"line":113,"column":99}}}) : helper)))
    + "</h2>\n                    ";
},"17":function(container,depth0,helpers,partials,data) {
    var helper, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "<h2 class=\"right__movie-rating negative-rating\">"
    + container.escapeExpression(((helper = (helper = lookupProperty(helpers,"rating") || (depth0 != null ? lookupProperty(depth0,"rating") : depth0)) != null ? helper : container.hooks.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"rating","hash":{},"data":data,"loc":{"start":{"line":114,"column":76},"end":{"line":114,"column":86}}}) : helper)))
    + "</h2>";
},"19":function(container,depth0,helpers,partials,data) {
    var stack1, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return ((stack1 = lookupProperty(helpers,"if").call(depth0 != null ? depth0 : (container.nullContext || {}),(depth0 != null ? lookupProperty(depth0,"userRating") : depth0),{"name":"if","hash":{},"fn":container.program(20, data, 0),"inverse":container.program(22, data, 0),"data":data,"loc":{"start":{"line":118,"column":20},"end":{"line":123,"column":27}}})) != null ? stack1 : "")
    + "                    <div class=\"movie-flex-right__rating-stars\">\n                        <input class=\"rating-stars__input-star ratings-stars__max-star\" id=\"star-10\" type=\"radio\" name=\"star\"/>\n                        <label class=\"rating-stars__label-star\" for=\"star-10\" data-rating=\"10\"></label>\n                        <input class=\"rating-stars__input-star\" id=\"star-9\" type=\"radio\" name=\"star\"/>\n                        <label class=\"rating-stars__label-star\" for=\"star-9\" data-rating=\"9\"></label>\n                        <input class=\"rating-stars__input-star\" id=\"star-8\" type=\"radio\" name=\"star\"/>\n                        <label class=\"rating-stars__label-star\" for=\"star-8\" data-rating=\"8\"></label>\n                        <input class=\"rating-stars__input-star\" id=\"star-7\" type=\"radio\" name=\"star\"/>\n                        <label class=\"rating-stars__label-star\" for=\"star-7\" data-rating=\"7\"></label>\n                        <input class=\"rating-stars__input-star\" id=\"star-6\" type=\"radio\" name=\"star\"/>\n                        <label class=\"rating-stars__label-star\" for=\"star-6\" data-rating=\"6\"></label>\n                        <input class=\"rating-stars__input-star\" id=\"star-5\" type=\"radio\" name=\"star\"/>\n                        <label class=\"rating-stars__label-star\" for=\"star-5\" data-rating=\"5\"></label>\n                        <input class=\"rating-stars__input-star\" id=\"star-4\" type=\"radio\" name=\"star\"/>\n                        <label class=\"rating-stars__label-star\" for=\"star-4\" data-rating=\"4\"></label>\n                        <input class=\"rating-stars__input-star\" id=\"star-3\" type=\"radio\" name=\"star\"/>\n                        <label class=\"rating-stars__label-star\" for=\"star-3\" data-rating=\"3\"></label>\n                        <input class=\"rating-stars__input-star\" id=\"star-2\" type=\"radio\" name=\"star\"/>\n                        <label class=\"rating-stars__label-star\" for=\"star-2\" data-rating=\"2\"></label>\n                        <input class=\"rating-stars__input-star\" id=\"star-1\" type=\"radio\" name=\"star\"/>\n                        <label class=\"rating-stars__label-star\" for=\"star-1\" data-rating=\"1\"></label>\n                    </div>\n";
},"20":function(container,depth0,helpers,partials,data) {
    var stack1, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "                        <h3 class=\"movie-flex-right__rate-movie\">Ваша оценка: "
    + container.escapeExpression(container.lambda(((stack1 = (depth0 != null ? lookupProperty(depth0,"userRating") : depth0)) != null ? lookupProperty(stack1,"score") : stack1), depth0))
    + "/10</h3>\n                        <p id=\"delete-rating\" class=\"movie-flex-right__delete-rating\">Удалить оценку</p>\n";
},"22":function(container,depth0,helpers,partials,data) {
    return "                        <h3 class=\"movie-flex-right__rate-movie\">Оцените фильм</h3>\n";
},"24":function(container,depth0,helpers,partials,data) {
    var alias1=container.lambda, alias2=container.escapeExpression, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "                            <li><a href='/actor/"
    + alias2(alias1((depth0 != null ? lookupProperty(depth0,"id") : depth0), depth0))
    + "' class=\"actors-list__item\">"
    + alias2(alias1((depth0 != null ? lookupProperty(depth0,"name") : depth0), depth0))
    + "</a></li>\n";
},"26":function(container,depth0,helpers,partials,data) {
    return "        <div class=\"main__content-container\">\n            <div class=\"content-container__similar-movies\">\n                <h3 class=\"main__movie-page-section-heading\">Похожие фильмы</h3>\n                <div id=\"similar-movies-carousel\" class=\"main__slider-wrapper\"></div>\n            </div>\n        </div>\n";
},"28":function(container,depth0,helpers,partials,data) {
    var stack1, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return ((stack1 = lookupProperty(helpers,"if").call(depth0 != null ? depth0 : (container.nullContext || {}),(depth0 != null ? lookupProperty(depth0,"userReview") : depth0),{"name":"if","hash":{},"fn":container.program(29, data, 0),"inverse":container.program(35, data, 0),"data":data,"loc":{"start":{"line":170,"column":8},"end":{"line":250,"column":15}}})) != null ? stack1 : "");
},"29":function(container,depth0,helpers,partials,data) {
    var stack1, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return ((stack1 = lookupProperty(helpers,"if").call(depth0 != null ? depth0 : (container.nullContext || {}),(depth0 != null ? lookupProperty(depth0,"wantsToEditReview") : depth0),{"name":"if","hash":{},"fn":container.program(30, data, 0),"inverse":container.program(33, data, 0),"data":data,"loc":{"start":{"line":171,"column":12},"end":{"line":222,"column":19}}})) != null ? stack1 : "");
},"30":function(container,depth0,helpers,partials,data) {
    var stack1, alias1=container.lambda, alias2=container.escapeExpression, alias3=depth0 != null ? depth0 : (container.nullContext || {}), alias4=container.hooks.helperMissing, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "                <div class=\"main__content-container\">\n                    <div class=\"content-container__write-review\" id=\"user-review-container\">\n                        <h3 class=\"main__movie-page-section-heading\">Редактирование рецензии</h3>\n                        <div class=\"write-review__review-form-container\">\n                            <form id=\"review\" class=\"review-form-container__review-form\">\n                                <div class=\"review-form-container__grid\">\n                                    <div>\n                                        <input name=\"title\" type=\"text\" class=\"form__input-field review-form__input-review-title\"\n                                               placeholder=\"Заголовок (до 200 символов)\" value=\""
    + alias2(alias1(((stack1 = (depth0 != null ? lookupProperty(depth0,"userReview") : depth0)) != null ? lookupProperty(stack1,"title") : stack1), depth0))
    + "\"\n                                               id=\"review-heading-input\">\n                                        <textarea name=\"content\" class=\"form__input-field review-form__input-review-content\"\n                                                  id=\"review-text-input\" placeholder=\"Ваши впечатления о фильме\">"
    + alias2(alias1(((stack1 = (depth0 != null ? lookupProperty(depth0,"userReview") : depth0)) != null ? lookupProperty(stack1,"content") : stack1), depth0))
    + "</textarea>\n                                    </div>\n                                    <select name=\"review_type\" class=\"review-form__select-review-type\" id=\"select-type\">\n                                        <option "
    + ((stack1 = lookupProperty(helpers,"if").call(alias3,(lookupProperty(helpers,"eq")||(depth0 && lookupProperty(depth0,"eq"))||alias4).call(alias3,((stack1 = (depth0 != null ? lookupProperty(depth0,"userReview") : depth0)) != null ? lookupProperty(stack1,"review_type") : stack1),"positive",{"name":"eq","hash":{},"data":data,"loc":{"start":{"line":186,"column":54},"end":{"line":186,"column":92}}}),{"name":"if","hash":{},"fn":container.program(31, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":186,"column":48},"end":{"line":186,"column":109}}})) != null ? stack1 : "")
    + "\n                                                value=\"positive\">Положительная\n                                        </option>\n                                        <option "
    + ((stack1 = lookupProperty(helpers,"if").call(alias3,(lookupProperty(helpers,"eq")||(depth0 && lookupProperty(depth0,"eq"))||alias4).call(alias3,((stack1 = (depth0 != null ? lookupProperty(depth0,"userReview") : depth0)) != null ? lookupProperty(stack1,"review_type") : stack1),"neutral",{"name":"eq","hash":{},"data":data,"loc":{"start":{"line":189,"column":54},"end":{"line":189,"column":91}}}),{"name":"if","hash":{},"fn":container.program(31, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":189,"column":48},"end":{"line":189,"column":108}}})) != null ? stack1 : "")
    + "\n                                                value=\"neutral\">Нейтральная\n                                        </option>\n                                        <option "
    + ((stack1 = lookupProperty(helpers,"if").call(alias3,(lookupProperty(helpers,"eq")||(depth0 && lookupProperty(depth0,"eq"))||alias4).call(alias3,((stack1 = (depth0 != null ? lookupProperty(depth0,"userReview") : depth0)) != null ? lookupProperty(stack1,"review_type") : stack1),"negative",{"name":"eq","hash":{},"data":data,"loc":{"start":{"line":192,"column":54},"end":{"line":192,"column":92}}}),{"name":"if","hash":{},"fn":container.program(31, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":192,"column":48},"end":{"line":192,"column":109}}})) != null ? stack1 : "")
    + "\n                                                value=\"negative\">Отрицательная\n                                        </option>\n                                    </select>\n                                </div>\n                                <p class=\"form__validation-hint form__send-form-hint\" id=\"validation-hint-review\"><em></em></p>\n                                <button type=\"submit\" id=\"review-button\"\n                                        class=\"form__submit-button form__submit-review\">Отправить</button>\n                            </form>\n                        </div>\n                    </div>\n                </div>\n";
},"31":function(container,depth0,helpers,partials,data) {
    return "selected";
},"33":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=container.lambda, alias2=container.escapeExpression, alias3=depth0 != null ? depth0 : (container.nullContext || {}), alias4=container.hooks.helperMissing, alias5="function", lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "                <div class=\"main__content-container\">\n                    <div class=\"content-container__review-content review_background_"
    + alias2(alias1(((stack1 = (depth0 != null ? lookupProperty(depth0,"userReview") : depth0)) != null ? lookupProperty(stack1,"review_type") : stack1), depth0))
    + "\">\n                        <h3 class=\"main__movie-page-section-heading\">\n                            Ваша рецензия\n                            <button class=\"fa-button your-review__edit-button\" id=\"edit-button\" data-movie-id=\""
    + alias2(((helper = (helper = lookupProperty(helpers,"id") || (depth0 != null ? lookupProperty(depth0,"id") : depth0)) != null ? helper : alias4),(typeof helper === alias5 ? helper.call(alias3,{"name":"id","hash":{},"data":data,"loc":{"start":{"line":209,"column":111},"end":{"line":209,"column":117}}}) : helper)))
    + "\">\n                                <i class=\"fa fa-pencil-square-o fa-2x\"></i>\n                            </button>\n                            <button class=\"fa-button your-review__delete-button\" id=\"delete-button\" data-movie-id=\""
    + alias2(((helper = (helper = lookupProperty(helpers,"id") || (depth0 != null ? lookupProperty(depth0,"id") : depth0)) != null ? helper : alias4),(typeof helper === alias5 ? helper.call(alias3,{"name":"id","hash":{},"data":data,"loc":{"start":{"line":212,"column":115},"end":{"line":212,"column":121}}}) : helper)))
    + "\">\n                                <i class=\"fa fa-trash-o fa-2x\"></i>\n                            </button>\n                        </h3>\n                        <div class=\"your-review__review-body\">\n                            <p class=\"review-body__title\">"
    + alias2(alias1(((stack1 = (depth0 != null ? lookupProperty(depth0,"userReview") : depth0)) != null ? lookupProperty(stack1,"title") : stack1), depth0))
    + "</p>\n                            <p class=\"review-body__content\">"
    + alias2(alias1(((stack1 = (depth0 != null ? lookupProperty(depth0,"userReview") : depth0)) != null ? lookupProperty(stack1,"content") : stack1), depth0))
    + "</p>\n                        </div>\n                    </div>\n                </div>\n";
},"35":function(container,depth0,helpers,partials,data) {
    return "            <div class=\"main__content-container\">\n                <div class=\"content-container__write-review\" id=\"user-review-container\">\n                    <h3 class=\"main__movie-page-section-heading\">Напишите рецензию</h3>\n                    <div class=\"write-review__review-form-container\">\n                        <form id=\"review\" class=\"review-form-container__review-form\">\n                            <div class=\"review-form-container__grid\">\n                                <div>\n                                    <input name=\"title\" type=\"text\" class=\"form__input-field review-form__input-review-title\"\n                                           placeholder=\"Заголовок (до 200 символов)\" id=\"review-heading-input\">\n                                    <textarea name=\"content\" class=\"form__input-field review-form__input-review-content\"\n                                              id=\"review-text-input\" placeholder=\"Ваши впечатления о фильме\"></textarea>\n                                </div>\n                                <select name=\"review_type\" class=\"review-form__select-review-type\" id=\"select-type\">\n                                    <option selected disabled>Тип рецензии</option>\n                                    <option value=\"positive\">Положительная</option>\n                                    <option value=\"neutral\">Нейтральная</option>\n                                    <option value=\"negative\">Отрицательная</option>\n                                </select>\n                            </div>\n                            <p class=\"form__validation-hint form__send-form-hint\" id=\"validation-hint-review\"><em></em></p>\n                            <button type=\"submit\" id=\"review-button\"\n                                    class=\"form__submit-button form__submit-review\">Отправить</button>\n                        </form>\n                    </div>\n                </div>\n            </div>\n";
},"37":function(container,depth0,helpers,partials,data,blockParams,depths) {
    var stack1, alias1=depth0 != null ? depth0 : (container.nullContext || {}), lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "        <hr class=\"main__reviews-separator\"/>\n\n"
    + ((stack1 = lookupProperty(helpers,"each").call(alias1,((stack1 = (depth0 != null ? lookupProperty(depth0,"reviewsData") : depth0)) != null ? lookupProperty(stack1,"reviews") : stack1),{"name":"each","hash":{},"fn":container.program(38, data, 0, blockParams, depths),"inverse":container.noop,"data":data,"loc":{"start":{"line":256,"column":8},"end":{"line":270,"column":17}}})) != null ? stack1 : "")
    + "\n"
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(lookupProperty(helpers,"notEq")||(depth0 && lookupProperty(depth0,"notEq"))||container.hooks.helperMissing).call(alias1,((stack1 = (depth0 != null ? lookupProperty(depth0,"reviewsData") : depth0)) != null ? lookupProperty(stack1,"pages_number") : stack1),1,{"name":"notEq","hash":{},"data":data,"loc":{"start":{"line":272,"column":14},"end":{"line":272,"column":48}}}),{"name":"if","hash":{},"fn":container.program(43, data, 0, blockParams, depths),"inverse":container.noop,"data":data,"loc":{"start":{"line":272,"column":8},"end":{"line":295,"column":15}}})) != null ? stack1 : "");
},"38":function(container,depth0,helpers,partials,data) {
    var stack1, alias1=container.lambda, alias2=container.escapeExpression, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return ((stack1 = lookupProperty(helpers,"if").call(depth0 != null ? depth0 : (container.nullContext || {}),(data && lookupProperty(data,"first")),{"name":"if","hash":{},"fn":container.program(39, data, 0),"inverse":container.program(41, data, 0),"data":data,"loc":{"start":{"line":257,"column":12},"end":{"line":261,"column":19}}})) != null ? stack1 : "")
    + "            <div class=\"content-container__review-content review_background_"
    + alias2(alias1((depth0 != null ? lookupProperty(depth0,"review_type") : depth0), depth0))
    + "\">\n                <h3 class=\"main__movie-page-section-heading\">Рецензия <a class=\"write-review__user-link\" href=\"/user/"
    + alias2(alias1((depth0 != null ? lookupProperty(depth0,"author") : depth0), depth0))
    + "\">"
    + alias2(alias1((depth0 != null ? lookupProperty(depth0,"author") : depth0), depth0))
    + "</a></h3>\n                <div class=\"review-content__review-body\">\n                    <p class=\"review-body__title\">"
    + alias2(alias1((depth0 != null ? lookupProperty(depth0,"title") : depth0), depth0))
    + "</p>\n                    <p class=\"review-body__content\">"
    + alias2(alias1((depth0 != null ? lookupProperty(depth0,"content") : depth0), depth0))
    + "</p>\n                </div>\n            </div>\n        </div>\n";
},"39":function(container,depth0,helpers,partials,data) {
    return "            <div class=\"main__content-container\" id=\"first-review\">\n";
},"41":function(container,depth0,helpers,partials,data) {
    return "            <div class=\"main__content-container\">\n";
},"43":function(container,depth0,helpers,partials,data,blockParams,depths) {
    var stack1, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return ((stack1 = (lookupProperty(helpers,"pagination")||(depth0 && lookupProperty(depth0,"pagination"))||container.hooks.helperMissing).call(depth0 != null ? depth0 : (container.nullContext || {}),((stack1 = (depth0 != null ? lookupProperty(depth0,"reviewsData") : depth0)) != null ? lookupProperty(stack1,"current_page") : stack1),((stack1 = (depth0 != null ? lookupProperty(depth0,"reviewsData") : depth0)) != null ? lookupProperty(stack1,"pages_number") : stack1),{"name":"pagination","hash":{},"fn":container.program(44, data, 0, blockParams, depths),"inverse":container.noop,"data":data,"loc":{"start":{"line":273,"column":12},"end":{"line":294,"column":27}}})) != null ? stack1 : "");
},"44":function(container,depth0,helpers,partials,data,blockParams,depths) {
    var stack1, alias1=depth0 != null ? depth0 : (container.nullContext || {}), lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "                <div class=\"main__pagination-wrapper\">\n"
    + ((stack1 = lookupProperty(helpers,"unless").call(alias1,(depth0 != null ? lookupProperty(depth0,"startFromFirstPage") : depth0),{"name":"unless","hash":{},"fn":container.program(45, data, 0, blockParams, depths),"inverse":container.noop,"data":data,"loc":{"start":{"line":275,"column":20},"end":{"line":278,"column":31}}})) != null ? stack1 : "")
    + "\n"
    + ((stack1 = lookupProperty(helpers,"each").call(alias1,(depth0 != null ? lookupProperty(depth0,"pages") : depth0),{"name":"each","hash":{},"fn":container.program(47, data, 0, blockParams, depths),"inverse":container.noop,"data":data,"loc":{"start":{"line":280,"column":20},"end":{"line":287,"column":29}}})) != null ? stack1 : "")
    + "\n"
    + ((stack1 = lookupProperty(helpers,"unless").call(alias1,(depth0 != null ? lookupProperty(depth0,"endAtLastPage") : depth0),{"name":"unless","hash":{},"fn":container.program(52, data, 0, blockParams, depths),"inverse":container.noop,"data":data,"loc":{"start":{"line":289,"column":20},"end":{"line":292,"column":31}}})) != null ? stack1 : "")
    + "                </div>\n";
},"45":function(container,depth0,helpers,partials,data,blockParams,depths) {
    var stack1, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "                        <button class=\"pagination-wrapper__button\" data-page-index=\""
    + container.escapeExpression((lookupProperty(helpers,"dec")||(depth0 && lookupProperty(depth0,"dec"))||container.hooks.helperMissing).call(depth0 != null ? depth0 : (container.nullContext || {}),((stack1 = (depths[1] != null ? lookupProperty(depths[1],"reviewsData") : depths[1])) != null ? lookupProperty(stack1,"current_page") : stack1),{"name":"dec","hash":{},"data":data,"loc":{"start":{"line":276,"column":84},"end":{"line":276,"column":119}}}))
    + "\">&lt;\n                        </button>\n";
},"47":function(container,depth0,helpers,partials,data) {
    var stack1, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return ((stack1 = lookupProperty(helpers,"if").call(depth0 != null ? depth0 : (container.nullContext || {}),(depth0 != null ? lookupProperty(depth0,"isCurrent") : depth0),{"name":"if","hash":{},"fn":container.program(48, data, 0),"inverse":container.program(50, data, 0),"data":data,"loc":{"start":{"line":281,"column":24},"end":{"line":286,"column":31}}})) != null ? stack1 : "");
},"48":function(container,depth0,helpers,partials,data) {
    var helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=container.hooks.helperMissing, alias3="function", alias4=container.escapeExpression, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "                            <button disabled class=\"pagination-wrapper__button\"\n                                    data-page-index=\""
    + alias4(((helper = (helper = lookupProperty(helpers,"page") || (depth0 != null ? lookupProperty(depth0,"page") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"page","hash":{},"data":data,"loc":{"start":{"line":283,"column":53},"end":{"line":283,"column":61}}}) : helper)))
    + "\">"
    + alias4(((helper = (helper = lookupProperty(helpers,"page") || (depth0 != null ? lookupProperty(depth0,"page") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"page","hash":{},"data":data,"loc":{"start":{"line":283,"column":63},"end":{"line":283,"column":71}}}) : helper)))
    + "</button>\n";
},"50":function(container,depth0,helpers,partials,data) {
    var helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=container.hooks.helperMissing, alias3="function", alias4=container.escapeExpression, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "                            <button class=\"pagination-wrapper__button\" data-page-index=\""
    + alias4(((helper = (helper = lookupProperty(helpers,"page") || (depth0 != null ? lookupProperty(depth0,"page") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"page","hash":{},"data":data,"loc":{"start":{"line":285,"column":88},"end":{"line":285,"column":96}}}) : helper)))
    + "\">"
    + alias4(((helper = (helper = lookupProperty(helpers,"page") || (depth0 != null ? lookupProperty(depth0,"page") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"page","hash":{},"data":data,"loc":{"start":{"line":285,"column":98},"end":{"line":285,"column":106}}}) : helper)))
    + "</button>\n";
},"52":function(container,depth0,helpers,partials,data,blockParams,depths) {
    var stack1, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "                        <button class=\"pagination-wrapper__button\" data-page-index=\""
    + container.escapeExpression((lookupProperty(helpers,"inc")||(depth0 && lookupProperty(depth0,"inc"))||container.hooks.helperMissing).call(depth0 != null ? depth0 : (container.nullContext || {}),((stack1 = (depths[1] != null ? lookupProperty(depths[1],"reviewsData") : depths[1])) != null ? lookupProperty(stack1,"current_page") : stack1),{"name":"inc","hash":{},"data":data,"loc":{"start":{"line":290,"column":84},"end":{"line":290,"column":119}}}))
    + "\">&gt;\n                        </button>\n";
},"compiler":[8,">= 4.3.0"],"main":function(container,depth0,helpers,partials,data,blockParams,depths) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=container.hooks.helperMissing, alias3="function", alias4=container.escapeExpression, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "<div id=\"navbar\"></div>\n\n\n<div class=\"main\">\n    <div class=\"main__content-container\">\n        <div class=\"content-container__movie-container\">\n            <div class=\"movie-container__background-trailer\">\n                <div class=\"background-trailer__img center\"\n                     style=\"background-image: url('"
    + alias4(((helper = (helper = lookupProperty(helpers,"banner") || (depth0 != null ? lookupProperty(depth0,"banner") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"banner","hash":{},"data":data,"loc":{"start":{"line":9,"column":51},"end":{"line":9,"column":61}}}) : helper)))
    + "');\"></div>\n            </div>\n\n            <div class=\"movie-container__left\">\n                <div class=\"movie-flex-left__inner\">\n                    <img class=\"inner__img_position_left\" src=\""
    + alias4(((helper = (helper = lookupProperty(helpers,"poster") || (depth0 != null ? lookupProperty(depth0,"poster") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"poster","hash":{},"data":data,"loc":{"start":{"line":14,"column":63},"end":{"line":14,"column":73}}}) : helper)))
    + "\">\n\n                    <iframe class=\"inner__img_position_left inner__trailer-container\" src=\""
    + alias4(((helper = (helper = lookupProperty(helpers,"trailer_preview") || (depth0 != null ? lookupProperty(depth0,"trailer_preview") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"trailer_preview","hash":{},"data":data,"loc":{"start":{"line":16,"column":91},"end":{"line":16,"column":110}}}) : helper)))
    + "\"\n                            title=\"YouTube player\" frameborder=\"0\" allow=\"accelerometer; autoplay; clipboard-write;\n                                encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>\n\n                </div>\n            </div>\n            <div class=\"movie-container__center\">\n                <div class=\"movie-flex-center__title-box\">\n                    <h1 class=\"title-box__title\">\n                        "
    + alias4(((helper = (helper = lookupProperty(helpers,"title") || (depth0 != null ? lookupProperty(depth0,"title") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"title","hash":{},"data":data,"loc":{"start":{"line":25,"column":24},"end":{"line":25,"column":33}}}) : helper)))
    + "\n                    </h1>\n                </div>\n                <div class=\"movie-flex-center__meta_info\">\n                    <p>"
    + alias4(((helper = (helper = lookupProperty(helpers,"description") || (depth0 != null ? lookupProperty(depth0,"description") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"description","hash":{},"data":data,"loc":{"start":{"line":29,"column":23},"end":{"line":29,"column":38}}}) : helper)))
    + "</p>\n                </div>\n\n                <h2 class=\"movie-flex-center__title-box\">О фильме</h2>\n\n                <div class=\"movie-flex-center__movie_info\">\n                    <div class=\"movie-info__info-row\">\n                        <div class=\"info-row__info-title\">Год производства</div>\n                        <a href=\"javascript:void(0);\" class=\"info-row__info-link\">"
    + alias4(((helper = (helper = lookupProperty(helpers,"production_year") || (depth0 != null ? lookupProperty(depth0,"production_year") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"production_year","hash":{},"data":data,"loc":{"start":{"line":37,"column":82},"end":{"line":37,"column":101}}}) : helper)))
    + "</a>\n                    </div>\n                    <div class=\"movie-info__info-row\">\n                        <div class=\"info-row__info-title\">Страна</div>\n"
    + ((stack1 = lookupProperty(helpers,"each").call(alias1,(depth0 != null ? lookupProperty(depth0,"country") : depth0),{"name":"each","hash":{},"fn":container.program(1, data, 0, blockParams, depths),"inverse":container.noop,"data":data,"loc":{"start":{"line":41,"column":24},"end":{"line":43,"column":33}}})) != null ? stack1 : "")
    + "                    </div>\n                    <div class=\"movie-info__info-row\">\n                        <div class=\"info-row__info-title\">Жанр</div>\n"
    + ((stack1 = lookupProperty(helpers,"each").call(alias1,(depth0 != null ? lookupProperty(depth0,"genre") : depth0),{"name":"each","hash":{},"fn":container.program(4, data, 0, blockParams, depths),"inverse":container.noop,"data":data,"loc":{"start":{"line":47,"column":24},"end":{"line":49,"column":33}}})) != null ? stack1 : "")
    + "                    </div>\n                    <div class=\"movie-info__info-row\">\n                        <div class=\"info-row__info-title\">Слоган</div>\n                        <div class=\"info_value\">«"
    + alias4(((helper = (helper = lookupProperty(helpers,"slogan") || (depth0 != null ? lookupProperty(depth0,"slogan") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"slogan","hash":{},"data":data,"loc":{"start":{"line":53,"column":49},"end":{"line":53,"column":59}}}) : helper)))
    + "»</div>\n                    </div>\n                    <div class=\"movie-info__info-row\">\n                        <div class=\"info-row__info-title\">Режиссёр</div>\n                        <a href=\"javascript:void(0);\" class=\"info-row__info-link\">"
    + alias4(((helper = (helper = lookupProperty(helpers,"director") || (depth0 != null ? lookupProperty(depth0,"director") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"director","hash":{},"data":data,"loc":{"start":{"line":57,"column":82},"end":{"line":57,"column":94}}}) : helper)))
    + "</a>\n                    </div>\n                    <div class=\"movie-info__info-row\">\n                        <div class=\"info-row__info-title\">Сценарист</div>\n                        <a href=\"javascript:void(0);\" class=\"info-row__info-link\">"
    + alias4(((helper = (helper = lookupProperty(helpers,"scriptwriter") || (depth0 != null ? lookupProperty(depth0,"scriptwriter") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"scriptwriter","hash":{},"data":data,"loc":{"start":{"line":61,"column":82},"end":{"line":61,"column":98}}}) : helper)))
    + "</a>\n                    </div>\n                    <div class=\"movie-info__info-row\">\n                        <div class=\"info-row__info-title\">Продюсер</div>\n                        <a href=\"javascript:void(0);\" class=\"info-row__info-link\">"
    + alias4(((helper = (helper = lookupProperty(helpers,"producer") || (depth0 != null ? lookupProperty(depth0,"producer") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"producer","hash":{},"data":data,"loc":{"start":{"line":65,"column":82},"end":{"line":65,"column":94}}}) : helper)))
    + "</a>\n                    </div>\n                    <div class=\"movie-info__info-row\">\n                        <div class=\"info-row__info-title\">Оператор</div>\n                        <a href=\"javascript:void(0);\" class=\"info-row__info-link\">"
    + alias4(((helper = (helper = lookupProperty(helpers,"operator") || (depth0 != null ? lookupProperty(depth0,"operator") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"operator","hash":{},"data":data,"loc":{"start":{"line":69,"column":82},"end":{"line":69,"column":94}}}) : helper)))
    + "</a>\n                    </div>\n                    <div class=\"movie-info__info-row\">\n                        <div class=\"info-row__info-title\">Композитор</div>\n                        <a href=\"javascript:void(0);\" class=\"info-row__info-link\">"
    + alias4(((helper = (helper = lookupProperty(helpers,"composer") || (depth0 != null ? lookupProperty(depth0,"composer") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"composer","hash":{},"data":data,"loc":{"start":{"line":73,"column":82},"end":{"line":73,"column":94}}}) : helper)))
    + "</a>\n                    </div>\n                    <div class=\"movie-info__info-row\">\n                        <div class=\"info-row__info-title\">Художник</div>\n                        <a href=\"javascript:void(0);\" class=\"info-row__info-link\">"
    + alias4(((helper = (helper = lookupProperty(helpers,"artist") || (depth0 != null ? lookupProperty(depth0,"artist") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"artist","hash":{},"data":data,"loc":{"start":{"line":77,"column":82},"end":{"line":77,"column":92}}}) : helper)))
    + "</a>\n                    </div>\n                    <div class=\"movie-info__info-row\">\n                        <div class=\"info-row__info-title\">Монтаж</div>\n                        <a href=\"javascript:void(0);\" class=\"info-row__info-link\">"
    + alias4(((helper = (helper = lookupProperty(helpers,"montage") || (depth0 != null ? lookupProperty(depth0,"montage") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"montage","hash":{},"data":data,"loc":{"start":{"line":81,"column":82},"end":{"line":81,"column":93}}}) : helper)))
    + "</a>\n                    </div>\n                    <div class=\"movie-info__info-row\">\n                        <div class=\"info-row__info-title\">Бюджет</div>\n                        <a href=\"javascript:void(0);\" class=\"info-row__info-link\">"
    + alias4(((helper = (helper = lookupProperty(helpers,"budget") || (depth0 != null ? lookupProperty(depth0,"budget") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"budget","hash":{},"data":data,"loc":{"start":{"line":85,"column":82},"end":{"line":85,"column":92}}}) : helper)))
    + "</a>\n                    </div>\n                    <div class=\"movie-info__info-row\">\n                        <div class=\"info-row__info-title\">Время</div>\n                        <div class=\"info_value\">"
    + alias4(((helper = (helper = lookupProperty(helpers,"duration") || (depth0 != null ? lookupProperty(depth0,"duration") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"duration","hash":{},"data":data,"loc":{"start":{"line":89,"column":48},"end":{"line":89,"column":60}}}) : helper)))
    + "</div>\n                    </div>\n                </div>\n            </div>\n            <div class=\"movie-container__right\">\n"
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(lookupProperty(helpers,"getAuthStatus")||(depth0 && lookupProperty(depth0,"getAuthStatus"))||alias2).call(alias1,{"name":"getAuthStatus","hash":{},"data":data,"loc":{"start":{"line":94,"column":22},"end":{"line":94,"column":37}}}),{"name":"if","hash":{},"fn":container.program(7, data, 0, blockParams, depths),"inverse":container.noop,"data":data,"loc":{"start":{"line":94,"column":16},"end":{"line":109,"column":23}}})) != null ? stack1 : "")
    + "\n                "
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(lookupProperty(helpers,"gte")||(depth0 && lookupProperty(depth0,"gte"))||alias2).call(alias1,(depth0 != null ? lookupProperty(depth0,"rating") : depth0),7,{"name":"gte","hash":{},"data":data,"loc":{"start":{"line":111,"column":22},"end":{"line":111,"column":36}}}),{"name":"if","hash":{},"fn":container.program(12, data, 0, blockParams, depths),"inverse":container.program(14, data, 0, blockParams, depths),"data":data,"loc":{"start":{"line":111,"column":16},"end":{"line":115,"column":23}}})) != null ? stack1 : "")
    + "\n"
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(lookupProperty(helpers,"getAuthStatus")||(depth0 && lookupProperty(depth0,"getAuthStatus"))||alias2).call(alias1,{"name":"getAuthStatus","hash":{},"data":data,"loc":{"start":{"line":117,"column":22},"end":{"line":117,"column":37}}}),{"name":"if","hash":{},"fn":container.program(19, data, 0, blockParams, depths),"inverse":container.noop,"data":data,"loc":{"start":{"line":117,"column":16},"end":{"line":146,"column":23}}})) != null ? stack1 : "")
    + "\n                <div class=\"movie-flex-right__main-roles\">\n                    <h3 class=\"main-roles__header\">В главных ролях</h3>\n                    <ul class=\"main-roles__actors-list\">\n"
    + ((stack1 = lookupProperty(helpers,"each").call(alias1,(depth0 != null ? lookupProperty(depth0,"actors") : depth0),{"name":"each","hash":{},"fn":container.program(24, data, 0, blockParams, depths),"inverse":container.noop,"data":data,"loc":{"start":{"line":151,"column":24},"end":{"line":153,"column":33}}})) != null ? stack1 : "")
    + "                    </ul>\n                </div>\n            </div>\n        </div>\n    </div>\n\n"
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"similarMovies") : depth0),{"name":"if","hash":{},"fn":container.program(26, data, 0, blockParams, depths),"inverse":container.noop,"data":data,"loc":{"start":{"line":160,"column":4},"end":{"line":167,"column":11}}})) != null ? stack1 : "")
    + "\n"
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(lookupProperty(helpers,"getAuthStatus")||(depth0 && lookupProperty(depth0,"getAuthStatus"))||alias2).call(alias1,{"name":"getAuthStatus","hash":{},"data":data,"loc":{"start":{"line":169,"column":10},"end":{"line":169,"column":25}}}),{"name":"if","hash":{},"fn":container.program(28, data, 0, blockParams, depths),"inverse":container.noop,"data":data,"loc":{"start":{"line":169,"column":4},"end":{"line":251,"column":11}}})) != null ? stack1 : "")
    + "\n"
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,((stack1 = (depth0 != null ? lookupProperty(depth0,"reviewsData") : depth0)) != null ? lookupProperty(stack1,"reviews") : stack1),{"name":"if","hash":{},"fn":container.program(37, data, 0, blockParams, depths),"inverse":container.noop,"data":data,"loc":{"start":{"line":253,"column":4},"end":{"line":296,"column":11}}})) != null ? stack1 : "")
    + "</div>\n";
},"useData":true,"useDepths":true});
})();