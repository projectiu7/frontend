(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['main.hbs'] = template({"1":function(container,depth0,helpers,partials,data) {
    return "                <div class=\"genres-list__item-box\">"
    + container.escapeExpression(container.lambda(depth0, depth0))
    + "</div>\n";
},"3":function(container,depth0,helpers,partials,data) {
    return "        <div id=\"genre-movies-carousel\" class=\"main__slider-wrapper\"></div>\n        <a href=\"/movies/genre/1/?filter=\" id=\"main-genre-search-button\" class=\"view-all\">смотреть все</a>\n";
},"5":function(container,depth0,helpers,partials,data) {
    var stack1, alias1=depth0 != null ? depth0 : (container.nullContext || {}), lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return ((stack1 = lookupProperty(helpers,"if").call(alias1,(lookupProperty(helpers,"notEq")||(depth0 && lookupProperty(depth0,"notEq"))||container.hooks.helperMissing).call(alias1,(depth0 != null ? lookupProperty(depth0,"movies_by_genres_preview") : depth0),undefined,{"name":"notEq","hash":{},"data":data,"loc":{"start":{"line":23,"column":14},"end":{"line":23,"column":56}}}),{"name":"if","hash":{},"fn":container.program(6, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":23,"column":8},"end":{"line":29,"column":15}}})) != null ? stack1 : "");
},"6":function(container,depth0,helpers,partials,data) {
    return "            <div class=\"main__message-empty\">\n                <p class=\"message-empty__text\">\n                    К сожалению, мы не нашли фильмы с таким фильтром!\n                </p>\n            </div>\n";
},"compiler":[8,">= 4.3.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, alias1=depth0 != null ? depth0 : (container.nullContext || {}), lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "<div id=\"navbar\"></div>\n\n\n<div class=\"main\">\n    <h3 class=\"main__section-name\">Лучшие фильмы</h3>\n\n    <div id=\"best-movies-carousel\" class=\"main__slider-wrapper\"></div>\n    <a class=\"view-all\" href=\"/movies/best/1\">смотреть все</a>\n\n    <h3 class=\"main__section-name\">По жанрам</h3>\n    <div class=\"main__genres-selection\">\n        <div class=\"main__genres-list\">\n"
    + ((stack1 = lookupProperty(helpers,"each").call(alias1,(depth0 != null ? lookupProperty(depth0,"available_genres") : depth0),{"name":"each","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":13,"column":12},"end":{"line":15,"column":21}}})) != null ? stack1 : "")
    + "        </div>\n    </div>\n\n"
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"movies_by_genres_preview") : depth0),{"name":"if","hash":{},"fn":container.program(3, data, 0),"inverse":container.program(5, data, 0),"data":data,"loc":{"start":{"line":19,"column":4},"end":{"line":30,"column":11}}})) != null ? stack1 : "")
    + "</div>\n";
},"useData":true});
})();