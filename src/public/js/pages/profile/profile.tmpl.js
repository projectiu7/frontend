(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['profile.hbs'] = template({"1":function(container,depth0,helpers,partials,data) {
    var stack1, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=container.hooks.helperMissing, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return ((stack1 = lookupProperty(helpers,"if").call(alias1,(lookupProperty(helpers,"notEq")||(depth0 && lookupProperty(depth0,"notEq"))||alias2).call(alias1,(depth0 != null ? lookupProperty(depth0,"username") : depth0),(lookupProperty(helpers,"getUsername")||(depth0 && lookupProperty(depth0,"getUsername"))||alias2).call(alias1,{"name":"getUsername","hash":{},"data":data,"loc":{"start":{"line":15,"column":50},"end":{"line":15,"column":63}}}),{"name":"notEq","hash":{},"data":data,"loc":{"start":{"line":15,"column":34},"end":{"line":15,"column":64}}}),{"name":"if","hash":{},"fn":container.program(2, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":15,"column":28},"end":{"line":23,"column":35}}})) != null ? stack1 : "");
},"2":function(container,depth0,helpers,partials,data) {
    var stack1, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return ((stack1 = lookupProperty(helpers,"if").call(depth0 != null ? depth0 : (container.nullContext || {}),(depth0 != null ? lookupProperty(depth0,"is_subscribed") : depth0),{"name":"if","hash":{},"fn":container.program(3, data, 0),"inverse":container.program(5, data, 0),"data":data,"loc":{"start":{"line":16,"column":32},"end":{"line":22,"column":39}}})) != null ? stack1 : "");
},"3":function(container,depth0,helpers,partials,data) {
    return "                                    <input type=\"checkbox\" id=\"follow\" class=\"head__follow-checkbox\" checked>\n                                    <label for=\"follow\" id=\"follow-button\" class=\"head__follow-button\">Отписаться</label>\n";
},"5":function(container,depth0,helpers,partials,data) {
    return "                                    <input type=\"checkbox\" id=\"follow\" class=\"head__follow-checkbox\">\n                                    <label for=\"follow\" id=\"follow-button\" class=\"head__follow-button\">Подписаться</label>\n";
},"7":function(container,depth0,helpers,partials,data) {
    return "                        <button id=\"button-profile-settings\" class=\"head__settings-button\">\n                            <a class=\"settings-button__settings-link\" href=\"/settings\">Настройки</a>\n                        </button>\n";
},"9":function(container,depth0,helpers,partials,data) {
    return "-->\n<!--        <h3 class=\"main__section-name\">Любимые фильмы</h3>-->\n<!--        <div id='favorite-movies-carousel' class=\"main__slider-wrapper\"></div>-->\n<!--    ";
},"11":function(container,depth0,helpers,partials,data) {
    return "        <h3 class=\"main__section-name\">Любимые актеры</h3>\n        <div id='favorite-actors-carousel' class=\"main__slider-wrapper\"></div>\n";
},"13":function(container,depth0,helpers,partials,data) {
    var stack1, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=container.hooks.helperMissing, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "        <div class=\"main__message-empty\">\n            <p id=\"no-playlists-message\" class=\"message-empty__text\">\n"
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(lookupProperty(helpers,"eq")||(depth0 && lookupProperty(depth0,"eq"))||alias2).call(alias1,(depth0 != null ? lookupProperty(depth0,"username") : depth0),(lookupProperty(helpers,"getUsername")||(depth0 && lookupProperty(depth0,"getUsername"))||alias2).call(alias1,{"name":"getUsername","hash":{},"data":data,"loc":{"start":{"line":67,"column":35},"end":{"line":67,"column":48}}}),{"name":"eq","hash":{},"data":data,"loc":{"start":{"line":67,"column":22},"end":{"line":67,"column":49}}}),{"name":"if","hash":{},"fn":container.program(14, data, 0),"inverse":container.program(16, data, 0),"data":data,"loc":{"start":{"line":67,"column":16},"end":{"line":71,"column":23}}})) != null ? stack1 : "")
    + "            </p>\n        </div>\n";
},"14":function(container,depth0,helpers,partials,data) {
    return "                    Создайте свой первый плейлист!\n";
},"16":function(container,depth0,helpers,partials,data) {
    return "                    У этого пользователя еще нет плейлистов\n";
},"18":function(container,depth0,helpers,partials,data) {
    return "        <h3 class=\"main__section-name\">Рецензии</h3>\n        <div id='reviews-carousel' class=\"main__slider-wrapper\"></div>\n";
},"compiler":[8,">= 4.3.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=container.hooks.helperMissing, alias3="function", alias4=container.escapeExpression, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "<div id=\"navbar\"></div>\n\n<div class=\"main\">\n    <div class=\"main__profile-card\">\n        <div class=\"profile-card__container\">\n            <div class=\"container__avatar-container\">\n                <img class=\"avatar-container__avatar\" src=\""
    + alias4(((helper = (helper = lookupProperty(helpers,"avatar") || (depth0 != null ? lookupProperty(depth0,"avatar") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"avatar","hash":{},"data":data,"loc":{"start":{"line":7,"column":59},"end":{"line":7,"column":69}}}) : helper)))
    + "\" alt=\"\">\n            </div>\n\n            <div class=\"profile-card__content\">\n                <div class=\"content__head\">\n                    <div class=\"head__first-row\">\n                        <p id=\"user-full-name\" class=\"head__user-name\">"
    + alias4(((helper = (helper = lookupProperty(helpers,"username") || (depth0 != null ? lookupProperty(depth0,"username") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"username","hash":{},"data":data,"loc":{"start":{"line":13,"column":71},"end":{"line":13,"column":83}}}) : helper)))
    + "</p>\n"
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(lookupProperty(helpers,"getAuthStatus")||(depth0 && lookupProperty(depth0,"getAuthStatus"))||alias2).call(alias1,{"name":"getAuthStatus","hash":{},"data":data,"loc":{"start":{"line":14,"column":30},"end":{"line":14,"column":45}}}),{"name":"if","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":14,"column":24},"end":{"line":24,"column":31}}})) != null ? stack1 : "")
    + "                    </div>\n                    <span id=\"user-email\" class=\"head__user-email\">"
    + alias4(((helper = (helper = lookupProperty(helpers,"email") || (depth0 != null ? lookupProperty(depth0,"email") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"email","hash":{},"data":data,"loc":{"start":{"line":26,"column":67},"end":{"line":26,"column":76}}}) : helper)))
    + "</span>\n"
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(lookupProperty(helpers,"eq")||(depth0 && lookupProperty(depth0,"eq"))||alias2).call(alias1,(depth0 != null ? lookupProperty(depth0,"username") : depth0),(lookupProperty(helpers,"getUsername")||(depth0 && lookupProperty(depth0,"getUsername"))||alias2).call(alias1,{"name":"getUsername","hash":{},"data":data,"loc":{"start":{"line":27,"column":39},"end":{"line":27,"column":52}}}),{"name":"eq","hash":{},"data":data,"loc":{"start":{"line":27,"column":26},"end":{"line":27,"column":53}}}),{"name":"if","hash":{},"fn":container.program(7, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":27,"column":20},"end":{"line":31,"column":27}}})) != null ? stack1 : "")
    + "                </div>\n                <div class=\"content__stats-grid\">\n                    <div class=\"stats__stats-item\">\n                        <p class=\"stats-item__name\">Посмотрел фильмов</p>\n                        <span id=\"movies-watched\" class=\"stats-item__value\">"
    + alias4(((helper = (helper = lookupProperty(helpers,"movies_watched") || (depth0 != null ? lookupProperty(depth0,"movies_watched") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"movies_watched","hash":{},"data":data,"loc":{"start":{"line":36,"column":76},"end":{"line":36,"column":94}}}) : helper)))
    + "</span>\n                    </div>\n                    <div class=\"stats__stats-item\">\n                        <p class=\"stats-item__name\">Рецензий</p>\n                        <span id=\"reviews-number\" class=\"stats-item__value\">"
    + alias4(((helper = (helper = lookupProperty(helpers,"reviews_number") || (depth0 != null ? lookupProperty(depth0,"reviews_number") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"reviews_number","hash":{},"data":data,"loc":{"start":{"line":40,"column":76},"end":{"line":40,"column":94}}}) : helper)))
    + "</span>\n                    </div>\n                    <div class=\"stats__stats-item\">\n                        <p class=\"stats-item__name\">Подписчиков</p>\n                        <span id=\"followers-number\" class=\"stats-item__value\">"
    + alias4(((helper = (helper = lookupProperty(helpers,"subscribers") || (depth0 != null ? lookupProperty(depth0,"subscribers") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"subscribers","hash":{},"data":data,"loc":{"start":{"line":44,"column":78},"end":{"line":44,"column":93}}}) : helper)))
    + "</span>\n                    </div>\n                    <div class=\"stats__stats-item\">\n                        <p class=\"stats-item__name\">Подписок</p>\n                        <span id=\"following-number\" class=\"stats-item__value\">"
    + alias4(((helper = (helper = lookupProperty(helpers,"subscriptions") || (depth0 != null ? lookupProperty(depth0,"subscriptions") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"subscriptions","hash":{},"data":data,"loc":{"start":{"line":48,"column":78},"end":{"line":48,"column":95}}}) : helper)))
    + "</span>\n                    </div>\n                </div>\n            </div>\n        </div>\n    </div>\n\n<!--    "
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"favorite_movies") : depth0),{"name":"if","hash":{},"fn":container.program(9, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":55,"column":8},"end":{"line":58,"column":15}}})) != null ? stack1 : "")
    + "-->\n"
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"favorite_actors") : depth0),{"name":"if","hash":{},"fn":container.program(11, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":59,"column":4},"end":{"line":62,"column":11}}})) != null ? stack1 : "")
    + "    <h3 class=\"main__section-name\">Плейлисты</h3>\n"
    + ((stack1 = lookupProperty(helpers,"unless").call(alias1,(depth0 != null ? lookupProperty(depth0,"playlists") : depth0),{"name":"unless","hash":{},"fn":container.program(13, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":64,"column":4},"end":{"line":74,"column":15}}})) != null ? stack1 : "")
    + "    <div id=\"accordion-container\"></div>\n\n"
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"reviews") : depth0),{"name":"if","hash":{},"fn":container.program(18, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":77,"column":4},"end":{"line":80,"column":11}}})) != null ? stack1 : "")
    + "</div>\n";
},"useData":true});
})();