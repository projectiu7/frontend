(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['feed.hbs'] = template({"1":function(container,depth0,helpers,partials,data) {
    return "        <div class=\"main__main-empty\">\n            Лента пуста\n        </div>\n";
},"3":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=container.hooks.helperMissing, alias3="function", alias4=container.escapeExpression, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "        <div class=\"main__feed-card\">\n            <div class=\"feed-card__header\">\n                <div class=\"feed-card__avatar-container\">\n                    <a href=\"/user/"
    + alias4(((helper = (helper = lookupProperty(helpers,"username") || (depth0 != null ? lookupProperty(depth0,"username") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"username","hash":{},"data":data,"loc":{"start":{"line":14,"column":35},"end":{"line":14,"column":47}}}) : helper)))
    + "\"><img class=\"feed-card__avatar\" src=\""
    + alias4(((helper = (helper = lookupProperty(helpers,"avatar") || (depth0 != null ? lookupProperty(depth0,"avatar") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"avatar","hash":{},"data":data,"loc":{"start":{"line":14,"column":85},"end":{"line":14,"column":95}}}) : helper)))
    + "\" alt=\"\"></a>\n                </div>\n                <h4 class=\"feed-card__username\"><a class=\"feed-card__user-link\" href=\"/user/"
    + alias4(((helper = (helper = lookupProperty(helpers,"username") || (depth0 != null ? lookupProperty(depth0,"username") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"username","hash":{},"data":data,"loc":{"start":{"line":16,"column":92},"end":{"line":16,"column":104}}}) : helper)))
    + "\">"
    + alias4(((helper = (helper = lookupProperty(helpers,"username") || (depth0 != null ? lookupProperty(depth0,"username") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"username","hash":{},"data":data,"loc":{"start":{"line":16,"column":106},"end":{"line":16,"column":118}}}) : helper)))
    + "</a></h4>\n                <h5 class=\"feed-card__action\">\n                    "
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(lookupProperty(helpers,"eq")||(depth0 && lookupProperty(depth0,"eq"))||alias2).call(alias1,(depth0 != null ? lookupProperty(depth0,"item_type") : depth0),"review",{"name":"eq","hash":{},"data":data,"loc":{"start":{"line":18,"column":26},"end":{"line":18,"column":49}}}),{"name":"if","hash":{},"fn":container.program(4, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":18,"column":20},"end":{"line":18,"column":74}}})) != null ? stack1 : "")
    + "\n                    "
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(lookupProperty(helpers,"eq")||(depth0 && lookupProperty(depth0,"eq"))||alias2).call(alias1,(depth0 != null ? lookupProperty(depth0,"item_type") : depth0),"rating",{"name":"eq","hash":{},"data":data,"loc":{"start":{"line":19,"column":26},"end":{"line":19,"column":49}}}),{"name":"if","hash":{},"fn":container.program(6, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":19,"column":20},"end":{"line":19,"column":73}}})) != null ? stack1 : "")
    + "\n                </h5>\n            </div>\n            <div class=\"feed-card__content\">\n"
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(lookupProperty(helpers,"eq")||(depth0 && lookupProperty(depth0,"eq"))||alias2).call(alias1,(depth0 != null ? lookupProperty(depth0,"item_type") : depth0),"review",{"name":"eq","hash":{},"data":data,"loc":{"start":{"line":23,"column":22},"end":{"line":23,"column":45}}}),{"name":"if","hash":{},"fn":container.program(8, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":23,"column":16},"end":{"line":30,"column":23}}})) != null ? stack1 : "")
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(lookupProperty(helpers,"eq")||(depth0 && lookupProperty(depth0,"eq"))||alias2).call(alias1,(depth0 != null ? lookupProperty(depth0,"item_type") : depth0),"rating",{"name":"eq","hash":{},"data":data,"loc":{"start":{"line":31,"column":22},"end":{"line":31,"column":45}}}),{"name":"if","hash":{},"fn":container.program(10, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":31,"column":16},"end":{"line":40,"column":23}}})) != null ? stack1 : "")
    + "            </div>\n        </div>\n";
},"4":function(container,depth0,helpers,partials,data) {
    return "написал рецензию";
},"6":function(container,depth0,helpers,partials,data) {
    return "поставил оценку";
},"8":function(container,depth0,helpers,partials,data) {
    var stack1, alias1=container.lambda, alias2=container.escapeExpression, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "                    <div class=\"content-container__review-content review_background_"
    + alias2(alias1(((stack1 = (depth0 != null ? lookupProperty(depth0,"review") : depth0)) != null ? lookupProperty(stack1,"review_type") : stack1), depth0))
    + "\">\n                        <div class=\"your-review__review-body\">\n                            <p class=\"review-body__title\">"
    + alias2(alias1(((stack1 = (depth0 != null ? lookupProperty(depth0,"review") : depth0)) != null ? lookupProperty(stack1,"title") : stack1), depth0))
    + "</p>\n                            <p class=\"review-body__content\">"
    + alias2(alias1(((stack1 = (depth0 != null ? lookupProperty(depth0,"review") : depth0)) != null ? lookupProperty(stack1,"content") : stack1), depth0))
    + "</p>\n                        </div>\n                    </div>\n";
},"10":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=container.hooks.helperMissing, alias3=container.escapeExpression, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "                    "
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(lookupProperty(helpers,"gte")||(depth0 && lookupProperty(depth0,"gte"))||alias2).call(alias1,((stack1 = (depth0 != null ? lookupProperty(depth0,"rating") : depth0)) != null ? lookupProperty(stack1,"score") : stack1),7,{"name":"gte","hash":{},"data":data,"loc":{"start":{"line":32,"column":26},"end":{"line":32,"column":46}}}),{"name":"if","hash":{},"fn":container.program(11, data, 0),"inverse":container.program(13, data, 0),"data":data,"loc":{"start":{"line":32,"column":20},"end":{"line":36,"column":27}}})) != null ? stack1 : "")
    + "                    <h5 class=\"feed-card__movie-rating-text\">фильму\n                        <a href=\"/movie/"
    + alias3(container.lambda(((stack1 = (depth0 != null ? lookupProperty(depth0,"rating") : depth0)) != null ? lookupProperty(stack1,"movie_id") : stack1), depth0))
    + "\" class=\"feed-card__movie-rating-link\">\""
    + alias3(((helper = (helper = lookupProperty(helpers,"movie_title") || (depth0 != null ? lookupProperty(depth0,"movie_title") : depth0)) != null ? helper : alias2),(typeof helper === "function" ? helper.call(alias1,{"name":"movie_title","hash":{},"data":data,"loc":{"start":{"line":38,"column":99},"end":{"line":38,"column":114}}}) : helper)))
    + "\"</a>\n                    </h5>\n";
},"11":function(container,depth0,helpers,partials,data) {
    var stack1, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "<h2 class=\"feed-card__movie-rating positive-rating\">"
    + container.escapeExpression(container.lambda(((stack1 = (depth0 != null ? lookupProperty(depth0,"rating") : depth0)) != null ? lookupProperty(stack1,"score") : stack1), depth0))
    + "</h2>\n";
},"13":function(container,depth0,helpers,partials,data) {
    var stack1, alias1=depth0 != null ? depth0 : (container.nullContext || {}), lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "                        "
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(lookupProperty(helpers,"gte")||(depth0 && lookupProperty(depth0,"gte"))||container.hooks.helperMissing).call(alias1,((stack1 = (depth0 != null ? lookupProperty(depth0,"rating") : depth0)) != null ? lookupProperty(stack1,"score") : stack1),4,{"name":"gte","hash":{},"data":data,"loc":{"start":{"line":34,"column":30},"end":{"line":34,"column":50}}}),{"name":"if","hash":{},"fn":container.program(14, data, 0),"inverse":container.program(16, data, 0),"data":data,"loc":{"start":{"line":34,"column":24},"end":{"line":35,"column":112}}})) != null ? stack1 : "")
    + "\n";
},"14":function(container,depth0,helpers,partials,data) {
    var stack1, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "<h2 class=\"feed-card__movie-rating neutral-rating\">"
    + container.escapeExpression(container.lambda(((stack1 = (depth0 != null ? lookupProperty(depth0,"rating") : depth0)) != null ? lookupProperty(stack1,"score") : stack1), depth0))
    + "</h2>\n                        ";
},"16":function(container,depth0,helpers,partials,data) {
    var stack1, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "<h2 class=\"feed-card__movie-rating negative-rating\">"
    + container.escapeExpression(container.lambda(((stack1 = (depth0 != null ? lookupProperty(depth0,"rating") : depth0)) != null ? lookupProperty(stack1,"score") : stack1), depth0))
    + "</h2>";
},"compiler":[8,">= 4.3.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, alias1=depth0 != null ? depth0 : (container.nullContext || {}), lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "<div id=\"navbar\"></div>\n\n<div class=\"main\">\n    <h3 class=\"main__page-title\">Последние действия</h3>\n"
    + ((stack1 = lookupProperty(helpers,"unless").call(alias1,(depth0 != null ? lookupProperty(depth0,"feed") : depth0),{"name":"unless","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":5,"column":4},"end":{"line":9,"column":15}}})) != null ? stack1 : "")
    + ((stack1 = lookupProperty(helpers,"each").call(alias1,(depth0 != null ? lookupProperty(depth0,"feed") : depth0),{"name":"each","hash":{},"fn":container.program(3, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":10,"column":4},"end":{"line":43,"column":13}}})) != null ? stack1 : "")
    + "</div>\n";
},"useData":true});
})();