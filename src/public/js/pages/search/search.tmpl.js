(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['search.hbs'] = template({"1":function(container,depth0,helpers,partials,data) {
    return "        <div class=\"main__main-empty\">\n            К сожалению, ничего не найдено!\n        </div>\n";
},"3":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=container.hooks.helperMissing, alias3="function", alias4=container.escapeExpression, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "        <div class=\"main__search-card\">\n            <div class=\"search-card__item-type\">Фильм</div>\n            <div class=\"search-card__left\">\n                <div class=\"search-card__avatar\">\n                    <a href=\"/movie/"
    + alias4(((helper = (helper = lookupProperty(helpers,"id") || (depth0 != null ? lookupProperty(depth0,"id") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"id","hash":{},"data":data,"loc":{"start":{"line":16,"column":36},"end":{"line":16,"column":42}}}) : helper)))
    + "\"><img class=\"search-card__image\" src=\""
    + alias4(((helper = (helper = lookupProperty(helpers,"poster") || (depth0 != null ? lookupProperty(depth0,"poster") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"poster","hash":{},"data":data,"loc":{"start":{"line":16,"column":81},"end":{"line":16,"column":91}}}) : helper)))
    + "\"></a>\n                </div>\n            </div>\n            <div class=\"search-card__right\">\n                <div class=\"search-card__title\">\n                    <a href=\"/movie/"
    + alias4(((helper = (helper = lookupProperty(helpers,"id") || (depth0 != null ? lookupProperty(depth0,"id") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"id","hash":{},"data":data,"loc":{"start":{"line":21,"column":36},"end":{"line":21,"column":42}}}) : helper)))
    + "\" class=\"search-card__link\">"
    + alias4(((helper = (helper = lookupProperty(helpers,"title") || (depth0 != null ? lookupProperty(depth0,"title") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"title","hash":{},"data":data,"loc":{"start":{"line":21,"column":70},"end":{"line":21,"column":79}}}) : helper)))
    + "</a>\n                </div>\n                <div class=\"search-card__movie-genres\">\n"
    + ((stack1 = lookupProperty(helpers,"each").call(alias1,(depth0 != null ? lookupProperty(depth0,"genre") : depth0),{"name":"each","hash":{},"fn":container.program(4, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":24,"column":20},"end":{"line":27,"column":29}}})) != null ? stack1 : "")
    + "                </div>\n            </div>\n        </div>\n";
},"4":function(container,depth0,helpers,partials,data) {
    var stack1, alias1=container.lambda, alias2=container.escapeExpression, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "                        <a href=\"/movies/genre/1/?filter="
    + alias2(alias1(depth0, depth0))
    + "\" class=\"movie-genres__genre-link center__info-link\">\n                            "
    + alias2(alias1(depth0, depth0))
    + "</a>"
    + ((stack1 = lookupProperty(helpers,"unless").call(depth0 != null ? depth0 : (container.nullContext || {}),(data && lookupProperty(data,"last")),{"name":"unless","hash":{},"fn":container.program(5, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":26,"column":40},"end":{"line":26,"column":83}}})) != null ? stack1 : "")
    + "\n";
},"5":function(container,depth0,helpers,partials,data) {
    return "<span>, </span>";
},"7":function(container,depth0,helpers,partials,data) {
    var helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=container.hooks.helperMissing, alias3="function", alias4=container.escapeExpression, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "        <div class=\"main__search-card\">\n            <div class=\"search-card__item-type\">Актер</div>\n            <div class=\"search-card__left\">\n                <div class=\"search-card__avatar\">\n                    <a href=\"/actor/"
    + alias4(((helper = (helper = lookupProperty(helpers,"id") || (depth0 != null ? lookupProperty(depth0,"id") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"id","hash":{},"data":data,"loc":{"start":{"line":37,"column":36},"end":{"line":37,"column":42}}}) : helper)))
    + "\"><img class=\"search-card__image\" src=\""
    + alias4(((helper = (helper = lookupProperty(helpers,"avatar") || (depth0 != null ? lookupProperty(depth0,"avatar") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"avatar","hash":{},"data":data,"loc":{"start":{"line":37,"column":81},"end":{"line":37,"column":91}}}) : helper)))
    + "\"></a>\n                </div>\n            </div>\n            <div class=\"search-card__right\">\n                <div class=\"search-card__title\">\n                    <a href=\"/actor/"
    + alias4(((helper = (helper = lookupProperty(helpers,"id") || (depth0 != null ? lookupProperty(depth0,"id") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"id","hash":{},"data":data,"loc":{"start":{"line":42,"column":36},"end":{"line":42,"column":42}}}) : helper)))
    + "\" class=\"search-card__link\">"
    + alias4(((helper = (helper = lookupProperty(helpers,"name") || (depth0 != null ? lookupProperty(depth0,"name") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"name","hash":{},"data":data,"loc":{"start":{"line":42,"column":70},"end":{"line":42,"column":78}}}) : helper)))
    + "</a>\n                </div>\n            </div>\n        </div>\n";
},"9":function(container,depth0,helpers,partials,data) {
    var helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=container.hooks.helperMissing, alias3="function", alias4=container.escapeExpression, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "        <div class=\"main__search-card\">\n            <div class=\"search-card__item-type\">Пользователь</div>\n            <div class=\"search-card__left\">\n                <div class=\"search-card__avatar\">\n                    <a href=\"/user/"
    + alias4(((helper = (helper = lookupProperty(helpers,"username") || (depth0 != null ? lookupProperty(depth0,"username") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"username","hash":{},"data":data,"loc":{"start":{"line":52,"column":35},"end":{"line":52,"column":47}}}) : helper)))
    + "\"><img class=\"search-card__image\" src=\""
    + alias4(((helper = (helper = lookupProperty(helpers,"avatar") || (depth0 != null ? lookupProperty(depth0,"avatar") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"avatar","hash":{},"data":data,"loc":{"start":{"line":52,"column":86},"end":{"line":52,"column":96}}}) : helper)))
    + "\"></a>\n                </div>\n            </div>\n            <div class=\"search-card__right\">\n                <div class=\"search-card__title\">\n                    <a href=\"/user/"
    + alias4(((helper = (helper = lookupProperty(helpers,"username") || (depth0 != null ? lookupProperty(depth0,"username") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"username","hash":{},"data":data,"loc":{"start":{"line":57,"column":35},"end":{"line":57,"column":47}}}) : helper)))
    + "\" class=\"search-card__link\">"
    + alias4(((helper = (helper = lookupProperty(helpers,"username") || (depth0 != null ? lookupProperty(depth0,"username") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"username","hash":{},"data":data,"loc":{"start":{"line":57,"column":75},"end":{"line":57,"column":87}}}) : helper)))
    + "</a>\n                </div>\n            </div>\n        </div>\n";
},"compiler":[8,">= 4.3.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, alias1=depth0 != null ? depth0 : (container.nullContext || {}), lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "<div id=\"navbar\"></div>\n\n<div class=\"main\">\n    <h3 class=\"main__page-title\">Результаты по запросу <span class=\"main__search-query\">\""
    + container.escapeExpression((lookupProperty(helpers,"decodeURI")||(depth0 && lookupProperty(depth0,"decodeURI"))||container.hooks.helperMissing).call(alias1,(depth0 != null ? lookupProperty(depth0,"query") : depth0),{"name":"decodeURI","hash":{},"data":data,"loc":{"start":{"line":4,"column":89},"end":{"line":4,"column":108}}}))
    + "\"</span></h3>\n\n"
    + ((stack1 = lookupProperty(helpers,"unless").call(alias1,(depth0 != null ? lookupProperty(depth0,"search_results") : depth0),{"name":"unless","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":6,"column":4},"end":{"line":10,"column":15}}})) != null ? stack1 : "")
    + ((stack1 = lookupProperty(helpers,"each").call(alias1,((stack1 = (depth0 != null ? lookupProperty(depth0,"search_results") : depth0)) != null ? lookupProperty(stack1,"movies") : stack1),{"name":"each","hash":{},"fn":container.program(3, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":11,"column":4},"end":{"line":31,"column":13}}})) != null ? stack1 : "")
    + ((stack1 = lookupProperty(helpers,"each").call(alias1,((stack1 = (depth0 != null ? lookupProperty(depth0,"search_results") : depth0)) != null ? lookupProperty(stack1,"actors") : stack1),{"name":"each","hash":{},"fn":container.program(7, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":32,"column":4},"end":{"line":46,"column":13}}})) != null ? stack1 : "")
    + ((stack1 = lookupProperty(helpers,"each").call(alias1,((stack1 = (depth0 != null ? lookupProperty(depth0,"search_results") : depth0)) != null ? lookupProperty(stack1,"users") : stack1),{"name":"each","hash":{},"fn":container.program(9, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":47,"column":4},"end":{"line":61,"column":13}}})) != null ? stack1 : "")
    + "</div>\n";
},"useData":true});
})();