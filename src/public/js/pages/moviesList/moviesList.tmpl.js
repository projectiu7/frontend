(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['moviesList.hbs'] = template({"1":function(container,depth0,helpers,partials,data) {
    return "        <h3 class=\"main__page-title\">Лучшие фильмы</h3>\n";
},"3":function(container,depth0,helpers,partials,data) {
    var stack1, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "        <h3 class=\"main__page-title\">\n            По жанрам:\n            "
    + ((stack1 = lookupProperty(helpers,"each").call(depth0 != null ? depth0 : (container.nullContext || {}),(depth0 != null ? lookupProperty(depth0,"genres") : depth0),{"name":"each","hash":{},"fn":container.program(4, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":11,"column":12},"end":{"line":11,"column":208}}})) != null ? stack1 : "")
    + "\n        </h3>\n";
},"4":function(container,depth0,helpers,partials,data) {
    var stack1, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=container.hooks.helperMissing, alias3=container.escapeExpression, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "<a href=\"/movies/genre/1/?filter="
    + alias3((lookupProperty(helpers,"decodeURI")||(depth0 && lookupProperty(depth0,"decodeURI"))||alias2).call(alias1,depth0,{"name":"decodeURI","hash":{},"data":data,"loc":{"start":{"line":11,"column":61},"end":{"line":11,"column":79}}}))
    + "\" class=\"genres-list__genre\">"
    + alias3((lookupProperty(helpers,"decodeURI")||(depth0 && lookupProperty(depth0,"decodeURI"))||alias2).call(alias1,depth0,{"name":"decodeURI","hash":{},"data":data,"loc":{"start":{"line":11,"column":108},"end":{"line":11,"column":126}}}))
    + "</a>"
    + ((stack1 = lookupProperty(helpers,"unless").call(alias1,(data && lookupProperty(data,"last")),{"name":"unless","hash":{},"fn":container.program(5, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":11,"column":130},"end":{"line":11,"column":199}}})) != null ? stack1 : "");
},"5":function(container,depth0,helpers,partials,data) {
    return "<span class=\"genres-list__genre\">/</span>";
},"7":function(container,depth0,helpers,partials,data,blockParams,depths) {
    var stack1, alias1=container.lambda, alias2=container.escapeExpression, alias3=depth0 != null ? depth0 : (container.nullContext || {}), alias4=container.hooks.helperMissing, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "            <div class=\"movies-list-grid__movie-card\">\n                <div class=\"movie-card__left\">\n                    <a href=\"/movie/"
    + alias2(alias1((depth0 != null ? lookupProperty(depth0,"id") : depth0), depth0))
    + "\"><img class=\"movie-card-img\" src=\""
    + alias2(alias1((depth0 != null ? lookupProperty(depth0,"poster") : depth0), depth0))
    + "\"></a>\n                </div>\n                <div class=\"movie-card__center\">\n                    <a class=\"center__movie-link center__info-link\" href=\"/movie/"
    + alias2(alias1((depth0 != null ? lookupProperty(depth0,"id") : depth0), depth0))
    + "\">\n                        <h3 class=\"center__card-title center__tooltip\"\n                            data-index=\""
    + alias2((lookupProperty(helpers,"calculateMovieIndex")||(depth0 && lookupProperty(depth0,"calculateMovieIndex"))||alias4).call(alias3,(depths[1] != null ? lookupProperty(depths[1],"current_page") : depths[1]),(depths[1] != null ? lookupProperty(depths[1],"max_items") : depths[1]),(lookupProperty(helpers,"inc")||(depth0 && lookupProperty(depth0,"inc"))||alias4).call(alias3,(data && lookupProperty(data,"index")),{"name":"inc","hash":{},"data":data,"loc":{"start":{"line":23,"column":91},"end":{"line":23,"column":103}}}),{"name":"calculateMovieIndex","hash":{},"data":data,"loc":{"start":{"line":23,"column":40},"end":{"line":23,"column":105}}}))
    + "\">\n                            "
    + alias2(alias1((depth0 != null ? lookupProperty(depth0,"title") : depth0), depth0))
    + "\n                        </h3>\n                        <span class=\"center__tooltip-text\">"
    + alias2(alias1((depth0 != null ? lookupProperty(depth0,"title") : depth0), depth0))
    + "</span>\n                    </a>\n                    <p class=\"center__movie-genres\">\n"
    + ((stack1 = lookupProperty(helpers,"each").call(alias3,(depth0 != null ? lookupProperty(depth0,"genre") : depth0),{"name":"each","hash":{},"fn":container.program(8, data, 0, blockParams, depths),"inverse":container.noop,"data":data,"loc":{"start":{"line":29,"column":24},"end":{"line":32,"column":33}}})) != null ? stack1 : "")
    + "                    </p>\n                    <p class=\"center__movie-description\">"
    + alias2(alias1((depth0 != null ? lookupProperty(depth0,"description") : depth0), depth0))
    + "</p>\n                </div>\n                <div class=\"movie-card__right\">\n                    "
    + ((stack1 = lookupProperty(helpers,"if").call(alias3,(lookupProperty(helpers,"gte")||(depth0 && lookupProperty(depth0,"gte"))||alias4).call(alias3,(depth0 != null ? lookupProperty(depth0,"rating") : depth0),7,{"name":"gte","hash":{},"data":data,"loc":{"start":{"line":37,"column":26},"end":{"line":37,"column":45}}}),{"name":"if","hash":{},"fn":container.program(11, data, 0, blockParams, depths),"inverse":container.program(13, data, 0, blockParams, depths),"data":data,"loc":{"start":{"line":37,"column":20},"end":{"line":41,"column":27}}})) != null ? stack1 : "")
    + ((stack1 = lookupProperty(helpers,"if").call(alias3,(lookupProperty(helpers,"getAuthStatus")||(depth0 && lookupProperty(depth0,"getAuthStatus"))||alias4).call(alias3,{"name":"getAuthStatus","hash":{},"data":data,"loc":{"start":{"line":42,"column":26},"end":{"line":42,"column":41}}}),{"name":"if","hash":{},"fn":container.program(18, data, 0, blockParams, depths),"inverse":container.noop,"data":data,"loc":{"start":{"line":42,"column":20},"end":{"line":57,"column":27}}})) != null ? stack1 : "")
    + "                </div>\n            </div>\n";
},"8":function(container,depth0,helpers,partials,data) {
    var stack1, alias1=container.lambda, alias2=container.escapeExpression, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "                            <a href=\"/movies/genre/1/?filter="
    + alias2(alias1(depth0, depth0))
    + "\" class=\"movie-genres__genre-link center__info-link\">\n                                "
    + alias2(alias1(depth0, depth0))
    + "</a>"
    + ((stack1 = lookupProperty(helpers,"unless").call(depth0 != null ? depth0 : (container.nullContext || {}),(data && lookupProperty(data,"last")),{"name":"unless","hash":{},"fn":container.program(9, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":31,"column":44},"end":{"line":31,"column":87}}})) != null ? stack1 : "")
    + "\n";
},"9":function(container,depth0,helpers,partials,data) {
    return "<span>, </span>";
},"11":function(container,depth0,helpers,partials,data) {
    var lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "<h4 class=\"right__movie-rating positive-rating\">"
    + container.escapeExpression(container.lambda((depth0 != null ? lookupProperty(depth0,"rating") : depth0), depth0))
    + "</h4>\n";
},"13":function(container,depth0,helpers,partials,data) {
    var stack1, alias1=depth0 != null ? depth0 : (container.nullContext || {}), lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "                        "
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(lookupProperty(helpers,"gte")||(depth0 && lookupProperty(depth0,"gte"))||container.hooks.helperMissing).call(alias1,(depth0 != null ? lookupProperty(depth0,"rating") : depth0),4,{"name":"gte","hash":{},"data":data,"loc":{"start":{"line":39,"column":30},"end":{"line":39,"column":49}}}),{"name":"if","hash":{},"fn":container.program(14, data, 0),"inverse":container.program(16, data, 0),"data":data,"loc":{"start":{"line":39,"column":24},"end":{"line":40,"column":107}}})) != null ? stack1 : "")
    + "\n";
},"14":function(container,depth0,helpers,partials,data) {
    var lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "<h4 class=\"right__movie-rating neutral-rating\">"
    + container.escapeExpression(container.lambda((depth0 != null ? lookupProperty(depth0,"rating") : depth0), depth0))
    + "</h4>\n                        ";
},"16":function(container,depth0,helpers,partials,data) {
    var lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "<h4 class=\"right__movie-rating negative-rating\">"
    + container.escapeExpression(container.lambda((depth0 != null ? lookupProperty(depth0,"rating") : depth0), depth0))
    + "</h4>";
},"18":function(container,depth0,helpers,partials,data) {
    var stack1, alias1=container.lambda, alias2=container.escapeExpression, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "                        <div class=\"right__bottom-buttons\">\n"
    + ((stack1 = lookupProperty(helpers,"if").call(depth0 != null ? depth0 : (container.nullContext || {}),(depth0 != null ? lookupProperty(depth0,"is_watched") : depth0),{"name":"if","hash":{},"fn":container.program(19, data, 0),"inverse":container.program(21, data, 0),"data":data,"loc":{"start":{"line":44,"column":28},"end":{"line":50,"column":35}}})) != null ? stack1 : "")
    + "                            <label class=\"buttons__label-watched\" for=\"watched-button-"
    + alias2(alias1((depth0 != null ? lookupProperty(depth0,"id") : depth0), depth0))
    + "\"></label>\n                            <input class=\"buttons__input-playlist buttons__movie-card-input-playlist\"\n                                   id=\"playlist-button-"
    + alias2(alias1((depth0 != null ? lookupProperty(depth0,"id") : depth0), depth0))
    + "\" type=\"checkbox\" name=\"playlist\" data-id=\""
    + alias2(alias1((depth0 != null ? lookupProperty(depth0,"id") : depth0), depth0))
    + "\">\n                            <label class=\"buttons__label-playlist\" for=\"playlist-button-"
    + alias2(alias1((depth0 != null ? lookupProperty(depth0,"id") : depth0), depth0))
    + "\"></label>\n                            <div class=\"buttons__add-to-playlist-container\" id=\"add-to-playlist-"
    + alias2(alias1((depth0 != null ? lookupProperty(depth0,"id") : depth0), depth0))
    + "\"></div>\n                        </div>\n";
},"19":function(container,depth0,helpers,partials,data) {
    var alias1=container.lambda, alias2=container.escapeExpression, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "                                <input class=\"buttons__input-watched buttons__movie-card-input-watched\" checked\n                                       id=\"watched-button-"
    + alias2(alias1((depth0 != null ? lookupProperty(depth0,"id") : depth0), depth0))
    + "\" type=\"checkbox\" name=\"watched\" data-id=\""
    + alias2(alias1((depth0 != null ? lookupProperty(depth0,"id") : depth0), depth0))
    + "\">\n";
},"21":function(container,depth0,helpers,partials,data) {
    var alias1=container.lambda, alias2=container.escapeExpression, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "                                <input class=\"buttons__input-watched buttons__movie-card-input-watched\"\n                                       id=\"watched-button-"
    + alias2(alias1((depth0 != null ? lookupProperty(depth0,"id") : depth0), depth0))
    + "\" type=\"checkbox\" name=\"watched\" data-id=\""
    + alias2(alias1((depth0 != null ? lookupProperty(depth0,"id") : depth0), depth0))
    + "\">\n";
},"23":function(container,depth0,helpers,partials,data,blockParams,depths) {
    var stack1, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return ((stack1 = (lookupProperty(helpers,"pagination")||(depth0 && lookupProperty(depth0,"pagination"))||container.hooks.helperMissing).call(depth0 != null ? depth0 : (container.nullContext || {}),(depth0 != null ? lookupProperty(depth0,"current_page") : depth0),(depth0 != null ? lookupProperty(depth0,"pages_number") : depth0),{"name":"pagination","hash":{},"fn":container.program(24, data, 0, blockParams, depths),"inverse":container.noop,"data":data,"loc":{"start":{"line":64,"column":8},"end":{"line":96,"column":23}}})) != null ? stack1 : "");
},"24":function(container,depth0,helpers,partials,data,blockParams,depths) {
    var stack1, alias1=depth0 != null ? depth0 : (container.nullContext || {}), lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "            <div class=\"main__pagination-wrapper\">\n"
    + ((stack1 = lookupProperty(helpers,"unless").call(alias1,(depth0 != null ? lookupProperty(depth0,"startFromFirstPage") : depth0),{"name":"unless","hash":{},"fn":container.program(25, data, 0, blockParams, depths),"inverse":container.noop,"data":data,"loc":{"start":{"line":66,"column":16},"end":{"line":74,"column":27}}})) != null ? stack1 : "")
    + "\n"
    + ((stack1 = lookupProperty(helpers,"each").call(alias1,(depth0 != null ? lookupProperty(depth0,"pages") : depth0),{"name":"each","hash":{},"fn":container.program(30, data, 0, blockParams, depths),"inverse":container.noop,"data":data,"loc":{"start":{"line":76,"column":16},"end":{"line":84,"column":25}}})) != null ? stack1 : "")
    + "\n"
    + ((stack1 = lookupProperty(helpers,"unless").call(alias1,(depth0 != null ? lookupProperty(depth0,"endAtLastPage") : depth0),{"name":"unless","hash":{},"fn":container.program(37, data, 0, blockParams, depths),"inverse":container.noop,"data":data,"loc":{"start":{"line":86,"column":16},"end":{"line":94,"column":27}}})) != null ? stack1 : "")
    + "            </div>\n";
},"25":function(container,depth0,helpers,partials,data,blockParams,depths) {
    var stack1, alias1=depth0 != null ? depth0 : (container.nullContext || {}), lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return ((stack1 = lookupProperty(helpers,"if").call(alias1,(lookupProperty(helpers,"eq")||(depth0 && lookupProperty(depth0,"eq"))||container.hooks.helperMissing).call(alias1,(depths[1] != null ? lookupProperty(depths[1],"type") : depths[1]),"best",{"name":"eq","hash":{},"data":data,"loc":{"start":{"line":67,"column":26},"end":{"line":67,"column":45}}}),{"name":"if","hash":{},"fn":container.program(26, data, 0, blockParams, depths),"inverse":container.program(28, data, 0, blockParams, depths),"data":data,"loc":{"start":{"line":67,"column":20},"end":{"line":71,"column":27}}})) != null ? stack1 : "")
    + "                        <button class=\"pagination-wrapper__button\">&lt;</button>\n                    </a>\n";
},"26":function(container,depth0,helpers,partials,data,blockParams,depths) {
    var lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "                        <a href=\"/movies/best/"
    + container.escapeExpression((lookupProperty(helpers,"dec")||(depth0 && lookupProperty(depth0,"dec"))||container.hooks.helperMissing).call(depth0 != null ? depth0 : (container.nullContext || {}),(depths[1] != null ? lookupProperty(depths[1],"current_page") : depths[1]),{"name":"dec","hash":{},"data":data,"loc":{"start":{"line":68,"column":46},"end":{"line":68,"column":69}}}))
    + "\">\n";
},"28":function(container,depth0,helpers,partials,data,blockParams,depths) {
    var alias1=container.escapeExpression, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "                        <a href=\"/movies/genre/"
    + alias1((lookupProperty(helpers,"dec")||(depth0 && lookupProperty(depth0,"dec"))||container.hooks.helperMissing).call(depth0 != null ? depth0 : (container.nullContext || {}),(depths[1] != null ? lookupProperty(depths[1],"current_page") : depths[1]),{"name":"dec","hash":{},"data":data,"loc":{"start":{"line":70,"column":47},"end":{"line":70,"column":70}}}))
    + "/"
    + alias1(container.lambda((depths[1] != null ? lookupProperty(depths[1],"query") : depths[1]), depth0))
    + "\">\n";
},"30":function(container,depth0,helpers,partials,data,blockParams,depths) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=container.hooks.helperMissing, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return ((stack1 = lookupProperty(helpers,"if").call(alias1,(lookupProperty(helpers,"eq")||(depth0 && lookupProperty(depth0,"eq"))||alias2).call(alias1,(depths[2] != null ? lookupProperty(depths[2],"type") : depths[2]),"best",{"name":"eq","hash":{},"data":data,"loc":{"start":{"line":77,"column":26},"end":{"line":77,"column":48}}}),{"name":"if","hash":{},"fn":container.program(31, data, 0, blockParams, depths),"inverse":container.program(33, data, 0, blockParams, depths),"data":data,"loc":{"start":{"line":77,"column":20},"end":{"line":81,"column":27}}})) != null ? stack1 : "")
    + "                    <button "
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"isCurrent") : depth0),{"name":"if","hash":{},"fn":container.program(35, data, 0, blockParams, depths),"inverse":container.noop,"data":data,"loc":{"start":{"line":82,"column":28},"end":{"line":82,"column":60}}})) != null ? stack1 : "")
    + " class=\"pagination-wrapper__button\">"
    + container.escapeExpression(((helper = (helper = lookupProperty(helpers,"page") || (depth0 != null ? lookupProperty(depth0,"page") : depth0)) != null ? helper : alias2),(typeof helper === "function" ? helper.call(alias1,{"name":"page","hash":{},"data":data,"loc":{"start":{"line":82,"column":96},"end":{"line":82,"column":104}}}) : helper)))
    + "</button>\n                    </a>\n";
},"31":function(container,depth0,helpers,partials,data) {
    var helper, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "                        <a href=\"/movies/best/"
    + container.escapeExpression(((helper = (helper = lookupProperty(helpers,"page") || (depth0 != null ? lookupProperty(depth0,"page") : depth0)) != null ? helper : container.hooks.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"page","hash":{},"data":data,"loc":{"start":{"line":78,"column":46},"end":{"line":78,"column":54}}}) : helper)))
    + "\">\n";
},"33":function(container,depth0,helpers,partials,data,blockParams,depths) {
    var helper, alias1=container.escapeExpression, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "                        <a href=\"/movies/genre/"
    + alias1(((helper = (helper = lookupProperty(helpers,"page") || (depth0 != null ? lookupProperty(depth0,"page") : depth0)) != null ? helper : container.hooks.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"page","hash":{},"data":data,"loc":{"start":{"line":80,"column":47},"end":{"line":80,"column":55}}}) : helper)))
    + "/"
    + alias1(container.lambda((depths[2] != null ? lookupProperty(depths[2],"query") : depths[2]), depth0))
    + "\">\n";
},"35":function(container,depth0,helpers,partials,data) {
    return "disabled";
},"37":function(container,depth0,helpers,partials,data,blockParams,depths) {
    var stack1, alias1=depth0 != null ? depth0 : (container.nullContext || {}), lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return ((stack1 = lookupProperty(helpers,"if").call(alias1,(lookupProperty(helpers,"eq")||(depth0 && lookupProperty(depth0,"eq"))||container.hooks.helperMissing).call(alias1,(depths[1] != null ? lookupProperty(depths[1],"type") : depths[1]),"best",{"name":"eq","hash":{},"data":data,"loc":{"start":{"line":87,"column":26},"end":{"line":87,"column":45}}}),{"name":"if","hash":{},"fn":container.program(38, data, 0, blockParams, depths),"inverse":container.program(40, data, 0, blockParams, depths),"data":data,"loc":{"start":{"line":87,"column":20},"end":{"line":91,"column":27}}})) != null ? stack1 : "")
    + "                    <button class=\"pagination-wrapper__button\">&gt;</button>\n                    </a>\n";
},"38":function(container,depth0,helpers,partials,data,blockParams,depths) {
    var lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "                        <a href=\"/movies/best/"
    + container.escapeExpression((lookupProperty(helpers,"inc")||(depth0 && lookupProperty(depth0,"inc"))||container.hooks.helperMissing).call(depth0 != null ? depth0 : (container.nullContext || {}),(depths[1] != null ? lookupProperty(depths[1],"current_page") : depths[1]),{"name":"inc","hash":{},"data":data,"loc":{"start":{"line":88,"column":46},"end":{"line":88,"column":69}}}))
    + "\">\n";
},"40":function(container,depth0,helpers,partials,data,blockParams,depths) {
    var alias1=container.escapeExpression, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "                        <a href=\"/movies/genre/"
    + alias1((lookupProperty(helpers,"inc")||(depth0 && lookupProperty(depth0,"inc"))||container.hooks.helperMissing).call(depth0 != null ? depth0 : (container.nullContext || {}),(depths[1] != null ? lookupProperty(depths[1],"current_page") : depths[1]),{"name":"inc","hash":{},"data":data,"loc":{"start":{"line":90,"column":47},"end":{"line":90,"column":70}}}))
    + "/"
    + alias1(container.lambda((depths[1] != null ? lookupProperty(depths[1],"query") : depths[1]), depth0))
    + "\">\n";
},"compiler":[8,">= 4.3.0"],"main":function(container,depth0,helpers,partials,data,blockParams,depths) {
    var stack1, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=container.hooks.helperMissing, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "<div id=\"navbar\"></div>\n\n\n<div class=\"main\">\n"
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(lookupProperty(helpers,"eq")||(depth0 && lookupProperty(depth0,"eq"))||alias2).call(alias1,(depth0 != null ? lookupProperty(depth0,"type") : depth0),"best",{"name":"eq","hash":{},"data":data,"loc":{"start":{"line":5,"column":10},"end":{"line":5,"column":26}}}),{"name":"if","hash":{},"fn":container.program(1, data, 0, blockParams, depths),"inverse":container.noop,"data":data,"loc":{"start":{"line":5,"column":4},"end":{"line":7,"column":11}}})) != null ? stack1 : "")
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(lookupProperty(helpers,"eq")||(depth0 && lookupProperty(depth0,"eq"))||alias2).call(alias1,(depth0 != null ? lookupProperty(depth0,"type") : depth0),"genres",{"name":"eq","hash":{},"data":data,"loc":{"start":{"line":8,"column":10},"end":{"line":8,"column":28}}}),{"name":"if","hash":{},"fn":container.program(3, data, 0, blockParams, depths),"inverse":container.noop,"data":data,"loc":{"start":{"line":8,"column":4},"end":{"line":13,"column":11}}})) != null ? stack1 : "")
    + "    <div class=\"main__movies-list-grid\">\n"
    + ((stack1 = lookupProperty(helpers,"each").call(alias1,(depth0 != null ? lookupProperty(depth0,"movies") : depth0),{"name":"each","hash":{},"fn":container.program(7, data, 0, blockParams, depths),"inverse":container.noop,"data":data,"loc":{"start":{"line":15,"column":8},"end":{"line":60,"column":17}}})) != null ? stack1 : "")
    + "    </div>\n\n"
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(lookupProperty(helpers,"notEq")||(depth0 && lookupProperty(depth0,"notEq"))||alias2).call(alias1,(depth0 != null ? lookupProperty(depth0,"pages_number") : depth0),1,{"name":"notEq","hash":{},"data":data,"loc":{"start":{"line":63,"column":10},"end":{"line":63,"column":32}}}),{"name":"if","hash":{},"fn":container.program(23, data, 0, blockParams, depths),"inverse":container.noop,"data":data,"loc":{"start":{"line":63,"column":4},"end":{"line":97,"column":11}}})) != null ? stack1 : "")
    + "</div>\n";
},"useData":true,"useDepths":true});
})();