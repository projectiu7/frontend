(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['actor.hbs'] = template({"1":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "                        <button id=\"actor-button-like\" class=\"left-inner__actor-button-like\" data-actor-id=\""
    + container.escapeExpression(((helper = (helper = lookupProperty(helpers,"id") || (depth0 != null ? lookupProperty(depth0,"id") : depth0)) != null ? helper : container.hooks.helperMissing),(typeof helper === "function" ? helper.call(alias1,{"name":"id","hash":{},"data":data,"loc":{"start":{"line":10,"column":108},"end":{"line":10,"column":114}}}) : helper)))
    + "\">\n"
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"is_liked") : depth0),{"name":"if","hash":{},"fn":container.program(2, data, 0),"inverse":container.program(4, data, 0),"data":data,"loc":{"start":{"line":11,"column":28},"end":{"line":15,"column":35}}})) != null ? stack1 : "")
    + "                        </button>\n";
},"2":function(container,depth0,helpers,partials,data) {
    return "                                Убрать из избранного\n";
},"4":function(container,depth0,helpers,partials,data) {
    return "                                В избранное\n";
},"6":function(container,depth0,helpers,partials,data) {
    var alias1=container.lambda, alias2=container.escapeExpression, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "                            <div class=\"movies-list__movies-item\">\n                                <a class=\"movies-item__title info-row__info-link\" href=\"/movie/"
    + alias2(alias1((depth0 != null ? lookupProperty(depth0,"id") : depth0), depth0))
    + "\">"
    + alias2(alias1((depth0 != null ? lookupProperty(depth0,"title") : depth0), depth0))
    + "</a>\n                                <p class=\"movies-item__rating\">Рейтинг: "
    + alias2(alias1((depth0 != null ? lookupProperty(depth0,"rating") : depth0), depth0))
    + "</p>\n                            </div>\n";
},"compiler":[8,">= 4.3.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=container.hooks.helperMissing, alias3="function", alias4=container.escapeExpression, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "<div id=\"navbar\"></div>\n\n<div class=\"main\">\n    <div class=\"main__actor-content\">\n        <div class=\"actor-content__grid-container\">\n            <div class=\"grid-container__left\">\n                <div class=\"grid-container__left-inner\">\n                    <img class=\"left-inner__actor-avatar-img\" src=\""
    + alias4(((helper = (helper = lookupProperty(helpers,"avatar") || (depth0 != null ? lookupProperty(depth0,"avatar") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"avatar","hash":{},"data":data,"loc":{"start":{"line":8,"column":67},"end":{"line":8,"column":77}}}) : helper)))
    + "\">\n"
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(lookupProperty(helpers,"getAuthStatus")||(depth0 && lookupProperty(depth0,"getAuthStatus"))||alias2).call(alias1,{"name":"getAuthStatus","hash":{},"data":data,"loc":{"start":{"line":9,"column":26},"end":{"line":9,"column":41}}}),{"name":"if","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":9,"column":20},"end":{"line":17,"column":27}}})) != null ? stack1 : "")
    + "                </div>\n            </div>\n\n            <div class=\"grid-container__center\">\n                <h1 class=\"center__actor-name\">\n                    "
    + alias4(((helper = (helper = lookupProperty(helpers,"name") || (depth0 != null ? lookupProperty(depth0,"name") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"name","hash":{},"data":data,"loc":{"start":{"line":23,"column":20},"end":{"line":23,"column":28}}}) : helper)))
    + "\n                </h1>\n\n                <div class=\"center__actor-info\">\n                    <div class=\"actor-info__header\">\n                        <h2 class=\"actor-content__header\">Основная информация</h2>\n                    </div>\n                    <div class=\"actor-info__info-row\">\n                        <div class=\"info-row__info-title\">Дата рождения</div>\n                        <div class=\"info-row__info-value\">"
    + alias4(((helper = (helper = lookupProperty(helpers,"birthdate") || (depth0 != null ? lookupProperty(depth0,"birthdate") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"birthdate","hash":{},"data":data,"loc":{"start":{"line":32,"column":58},"end":{"line":32,"column":71}}}) : helper)))
    + "</div>\n                    </div>\n                    <div class=\"actor-info__info-row\">\n                        <div class=\"info-row__info-title\">Страна</div>\n                        <div class=\"info-row__info-value\">"
    + alias4(((helper = (helper = lookupProperty(helpers,"origin") || (depth0 != null ? lookupProperty(depth0,"origin") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"origin","hash":{},"data":data,"loc":{"start":{"line":36,"column":58},"end":{"line":36,"column":68}}}) : helper)))
    + "</div>\n                    </div>\n\n                    <div class=\"actor-info__info-row\">\n                        <div class=\"info-row__info-title\">Профессия</div>\n                        <div class=\"info-row__info-value\">"
    + alias4(((helper = (helper = lookupProperty(helpers,"profession") || (depth0 != null ? lookupProperty(depth0,"profession") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"profession","hash":{},"data":data,"loc":{"start":{"line":41,"column":58},"end":{"line":41,"column":72}}}) : helper)))
    + "</div>\n                    </div>\n\n                    <div class=\"actor-info__info-row\">\n                        <div class=\"info-row__info-title\">Всего фильмов</div>\n                        <p class=\"info-row__info-value\">"
    + alias4(((helper = (helper = lookupProperty(helpers,"movies_count") || (depth0 != null ? lookupProperty(depth0,"movies_count") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"movies_count","hash":{},"data":data,"loc":{"start":{"line":46,"column":56},"end":{"line":46,"column":72}}}) : helper)))
    + "</p>\n                    </div>\n<!--                    <div class=\"actor-info__info-row\">-->\n<!--                        <div class=\"info-row__info-title\">Средний рейтинг фильмов</div>-->\n<!--                        <p class=\"info-row__info-value\">"
    + alias4(((helper = (helper = lookupProperty(helpers,"movies_rating") || (depth0 != null ? lookupProperty(depth0,"movies_rating") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"movies_rating","hash":{},"data":data,"loc":{"start":{"line":50,"column":60},"end":{"line":50,"column":77}}}) : helper)))
    + "</p>-->\n<!--                    </div>-->\n\n                    <div class=\"actor-info__biography\">\n                        <h2 class=\"actor-content__header\">Факты</h2>\n                        <div class=\"biography__text-container\">\n                            <p class=\"biography__text\">"
    + alias4(((helper = (helper = lookupProperty(helpers,"biography") || (depth0 != null ? lookupProperty(depth0,"biography") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"biography","hash":{},"data":data,"loc":{"start":{"line":56,"column":55},"end":{"line":56,"column":68}}}) : helper)))
    + "</p>\n                        </div>\n                    </div>\n\n                </div>\n\n                <div class=\"center__actor-movies\">\n                    <h2 class=\"actor-content__header\">Фильмы</h2>\n                    <div class=\"actor-movies__movies-list\">\n"
    + ((stack1 = lookupProperty(helpers,"each").call(alias1,(depth0 != null ? lookupProperty(depth0,"movies") : depth0),{"name":"each","hash":{},"fn":container.program(6, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":65,"column":24},"end":{"line":70,"column":33}}})) != null ? stack1 : "")
    + "                    </div>\n                </div>\n            </div>\n        </div>\n    </div>\n</div>\n\n";
},"useData":true});
})();